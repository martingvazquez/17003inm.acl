import application
from models import models
import uuid

db = application.db
ap = application.app

applications_insert = []

applications_insert = [{'description': 'ACL', 'notes': '', 'port': '5000'},
                       {'description': 'INMOBILIARIA', 'notes': '', 'port': '5001'}]


for application_insert in applications_insert:
    description = application_insert['description']
    notes = application_insert['notes']
    port = application_insert['port']

    applications = db.session.query(models.Applications)
    applications = applications.filter(models.Applications.description == description)
    n_applications = applications.count()
    applications = applications.first()

    if n_applications == 0:
        applications = models.Applications(description=description, notes=notes, uuid=uuid.uuid4(),
                                       port=port)
        try:
            db.session.add(applications)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            print(e)

countries_insert = []
countries_insert = [{'description': 'MEXICO', 'notes': ''},
                    {'description': 'GUATEMALA', 'notes': ''}]
for country_insert in countries_insert:

    description = country_insert['description']
    notes = country_insert['notes']

    countries = db.session.query(models.Countries)
    countries = countries.filter(models.Countries.description == description)
    n_countries = countries.count()
    countries = countries.first()

    if n_countries == 0:
        countries = models.Countries(description=description, notes=notes, uuid=uuid.uuid4())
        try:
            db.session.add(countries)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            print(e)
            

groups_insert = []
groups_insert = [{'description': 'ADMINS', 'notes': ''}]
for group_insert in groups_insert:

    description = group_insert['description']
    notes = group_insert['notes']

    groups = db.session.query(models.Groups)
    groups = groups.filter(models.Groups.description == description)
    n_groups = groups.count()
    groups = groups.first()

    if n_groups == 0:
        groups = models.Groups(description=description, notes=notes, uuid=uuid.uuid4())
        try:
            db.session.add(groups)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            print(e)


applications = db.session.query(models.Applications.id, models.Applications.description).all()
for application in applications:
    application_description = application.description
    if application_description == 'ACL':
        resources_insert = []
        resources_insert = [{'description': 'APPLICATIONS', 'notes': ''},
                            {'description': 'CITIES', 'notes': ''},
                            {'description': 'COMPANIES', 'notes': ''},
                            {'description': 'GROUPS', 'notes': ''},
                            {'description': 'GROUPS_URLS', 'notes': ''},
                            {'description': 'RESOURCES', 'notes': ''},
                            {'description': 'SESSIONS', 'notes': ''},
                            {'description': 'STATES', 'notes': ''},
                            {'description': 'URLS', 'notes': ''},
                            {'description': 'USERS', 'notes': ''},
                            {'description': 'COUNTRIES', 'notes': ''},
                            {'description': 'AGENT_TYPES', 'notes': ''},
                            {'description': 'AGENTS', 'notes': ''},
                            {'description': 'CLIENTS', 'notes': ''},
                            {'description': 'CLIENT_TYPES', 'notes': ''},
                            {'description': 'CONTRACTOR_TYPES', 'notes': ''},
                            {'description': 'CONTRACTORS', 'notes': ''},
                            {'description': 'DOCUMENT_TYPES', 'notes': ''},
                            {'description': 'DOCUMENTS', 'notes': ''},
                            {'description': 'FINANCING_TYPES', 'notes': ''},
                            {'description': 'FINANCINGS', 'notes': ''},
                            {'description': 'LOCATIONS', 'notes': ''},
                            {'description': 'PROJECT_TYPES', 'notes': ''},
                            {'description': 'PROJECTS', 'notes': ''},
                            {'description': 'PHASES', 'notes': ''}]
        for resource_insert in resources_insert:

            description = resource_insert['description']
            notes = resource_insert['notes']
            application_id = application.id

            resources = db.session.query(models.Resources)
            resources = resources.filter(models.Resources.description == description)
            n_resources = resources.count()
            resources = resources.first()

            if n_resources == 0:
                resources = models.Resources(description=description, notes=notes,
                                             uuid=uuid.uuid4(), application_id=application_id)
                try:
                    db.session.add(resources)
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    print(e)


countries = db.session.query(models.Countries.id, models.Countries.description).all()

for country in countries:
    country_description = country.description
    if country_description == 'MEXICO':
        states_insert = []
        states_insert = [{'description': 'JALISCO', 'notes': ''},
                         {'description': 'COLIMA', 'notes': ''},
                         {'description': 'AGUASCALIENTES', 'notes': ''},
                         {'description': 'MICHOACAN', 'notes': ''},
                         {'description': 'ZACATECAS', 'notes': ''},
                         {'description': 'NAYARIT', 'notes': ''},
                         {'description': 'YUCATAN', 'notes': ''},
                         {'description': 'EDO DE MEXICO', 'notes': ''},
                         {'description': 'CIUDAD DE MEXICO', 'notes': ''},
                         {'description': 'SINALOA', 'notes': ''}]
        for state_insert in states_insert:

            description = state_insert['description']
            notes = state_insert['notes']
            country_id = country.id

            states = db.session.query(models.States)
            states = states.filter(models.States.description == description)
            n_states = states.count()
            states = states.first()

            if n_states == 0:
                states = models.States(description=description, notes=notes,
                                       uuid=uuid.uuid4(), country_id=country_id)
                try:
                    db.session.add(states)
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    print(e)


resources = db.session.query(models.Resources.id, models.Resources.description).\
    filter(models.Applications.description == 'ACL').all()

for resource in resources:
    resource_description = resource.description.lower()
    urls_insert = []
    urls_insert = [{'description': '/acl/' + resource_description, 'notes': '', 'method': 'get'},
                   {'description': '/acl/' + resource_description, 'notes': '', 'method': 'post'},
                   {'description': '/acl/' + resource_description, 'notes': '', 'method': 'patch'},
                   {'description': '/acl/' + resource_description, 'notes': '', 'method': 'put'}]
    for url_insert in urls_insert:
        description = url_insert['description']
        notes = url_insert['notes']
        method = url_insert['method']
        resource_id = resource.id

        urls = db.session.query(models.Urls)
        urls = urls.filter(models.Urls.description == description)
        urls = urls.filter(models.Urls.method == method)
        n_urls = urls.count()
        urls = urls.first()
        if n_urls == 0:
            urls = models.Urls(description=description, notes=notes, uuid=uuid.uuid4(),
                               resource_id=resource_id, method=method)
            try:
                db.session.add(urls)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                print(e)


groups = db.session.query(models.Groups.id, models.Groups.description).all()

for group in groups:
    urls = db.session.query(models.Urls.id,models.Urls.description).all()
    for url in urls:
        group_id = group.id
        url_id = url.id

        urls_groups = db.session.query(models.UrlsGroups)
        urls_groups = urls_groups.filter(models.UrlsGroups.group_id == group_id)
        urls_groups = urls_groups.filter(models.UrlsGroups.url_id == url_id)
        n_urls_groups = urls_groups.count()

        if n_urls_groups == 0:
            urls_groups = models.UrlsGroups(url_id=url_id, group_id=group_id)
            try:
                db.session.add(urls_groups)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                print(e)


users_insert = []

users_insert = [{'description': 'USER ROOT', 'notes': '', 'name': 'ADMIN SYS', 'last_name': 'ROOT',
                 'second_last_name': 'ROOT', 'user': 'root', 'password': 'moro58'}]


for user_insert in users_insert:

    groups = db.session.query(models.Groups.id, models.Groups.description)
    groups = groups.filter(models.Groups.description == 'ADMINS')
    groups = groups.first()

    group_id = groups.id
    description = user_insert['description']
    notes = user_insert['notes']
    name = user_insert['name']
    last_name = user_insert['last_name']
    second_last_name = user_insert['second_last_name']
    user = user_insert['user']
    password = user_insert['password']

    users = db.session.query(models.Users)
    users = users.filter(models.Users.user == user)
    n_users = users.count()
    users = users.first()

    if n_users == 0:
        users = models.Users(description=description, notes=notes, group_id=group_id,
                             name=name, last_name=last_name, second_last_name=second_last_name,
                             user=user, password=password, uuid=uuid.uuid4())
        try:
            db.session.add(users)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            print(e)


states = db.session.query(models.States.id, models.States.description).all()

for state in states:
    state_description = state.description
    if state_description == 'JALISCO':
        cities_insert = []
        cities_insert = [{'description': 'GUADALAJARA', 'notes': ''},
                         {'description': 'ZAPOPAN', 'notes': ''},
                         {'description': 'CD GUZMAN', 'notes': ''},
                         {'description': 'PUERTO VALLARTA', 'notes': ''},
                         {'description': 'TONALA', 'notes': ''},
                         {'description': 'TLAQUEPAQUE', 'notes': ''},
                         {'description': 'TLAJOMULCO DE ZUÑIGA', 'notes': ''},
                         {'description': 'ZAPOTLANEJO', 'notes': ''},
                         {'description': 'TEPATITLAN', 'notes': ''},
                         {'description': 'AMECA', 'notes': ''}]
        for city_insert in cities_insert:

            description = city_insert['description']
            notes = city_insert['notes']
            state_id = state.id

            cities = db.session.query(models.States)
            cities = cities.filter(models.States.description == description)
            n_cities = cities.count()
            cities = cities.first()

            if n_cities == 0:
                cities = models.Cities(description=description, notes=notes, state_id=state_id, uuid=uuid.uuid4())
                try:
                    db.session.add(cities)
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    print(e)
