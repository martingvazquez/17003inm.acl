import uuid
import application


db = application.db
ma = application.ma
datetime = application.datetime
UUID = application.UUID
fields = application.fields
UniqueConstraint = application.UniqueConstraint


class Companies(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), unique=True, nullable=False)
    notes = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    city_id = db.Column(db.BigInteger, db.ForeignKey('cities.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    city = db.relationship('Cities', backref=db.backref("companies"))

    def __init__(self, description, notes, city_id, uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.city_id = city_id


class Countries(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), unique=True, nullable=False)
    notes = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)

    def __init__(self, description, notes, uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid


class Applications(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), unique=True, nullable=False)
    notes = db.Column(db.String)
    port = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)

    def __init__(self, description, notes, uuid, port):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.port = port


class Groups(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), unique=True, nullable=False)
    notes = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)

    def __init__(self, description, notes, uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid



class States(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False)
    notes = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    country_id = db.Column(db.BigInteger, db.ForeignKey('countries.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    country = db.relationship('Countries', backref=db.backref("states"))

    def __init__(self, description, notes, country_id, uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.country_id = country_id


class Users(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    user = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(500), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    second_last_name = db.Column(db.String(100))
    description = db.Column(db.String(100), nullable=False)
    notes = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    group_id = db.Column(db.BigInteger, db.ForeignKey('groups.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    group = db.relationship('Groups', backref=db.backref("users"))

    def __init__(self, description, name, last_name, second_last_name, user, password, notes, group_id, uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.group_id = group_id
        self.name = name
        self.last_name = last_name
        self.second_last_name = second_last_name
        self.user = user
        self.password = password


class Cities(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False)
    notes = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    state_id = db.Column(db.BigInteger, db.ForeignKey('states.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    state = db.relationship('States', backref=db.backref("cities"))

    def __init__(self, description, notes, state_id, uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.state_id = state_id


class Urls(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False)
    notes = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    method = db.Column(db.String(100), nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    resource_id = db.Column(db.BigInteger, db.ForeignKey('resources.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    resource = db.relationship('Resources', backref=db.backref("urls"))

    def __init__(self, description, notes, resource_id, method, uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.resource_id = resource_id
        self.method = method


class UrlsGroups(db.Model):
    uuid_insert = uuid.uuid4()
    __tablename__ = 'urls_groups'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    group_id = db.Column(db.BigInteger, db.ForeignKey('groups.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    groups = db.relationship('Groups', backref=db.backref("groups_urls"))
    url_id = db.Column(db.BigInteger, db.ForeignKey('urls.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    urls = db.relationship('Urls', backref=db.backref("groups_urls"))
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)


    def __init__(self,group_id, url_id):
        self.group_id = group_id
        self.url_id = url_id


class Tokens(db.Model):
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    group_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    user_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    token = db.Column(db.String(500), nullable=False, unique=True)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)

    def __init__(self, group_uuid, user_uuid, token):
        self.group_uuid = group_uuid
        self.user_uuid = user_uuid
        self.token = token

class Resources(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.BigInteger, default=1, nullable=False)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    insert_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    update_user_uuid = db.Column(UUID(as_uuid=True), default=uuid_insert, nullable=False)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    application_id = db.Column(db.BigInteger, db.ForeignKey('applications.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    application = db.relationship('Applications', backref=db.backref("resources"))

    def __init__(self, description, notes, application_id, uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.application_id = application_id

##++++++++++++++++++ CLIENTS ++++++++++++++++++++++++++++
class Clients(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    city_uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    second_last_name = db.Column(db.String(100))
    phone_number = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100))
    curp = db.Column(db.String(100), unique=True)
    rfc = db.Column(db.String(100), unique=True)
    gender = db.Column(db.String(100), nullable=False)
    birth_date = db.Column(db.String(100), nullable=False)
    place_of_birth = db.Column(db.String(100), nullable=False)
    purchase_range_initial = db.Column(db.Integer)
    purchase_range_final = db.Column(db.Integer)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    client_type_id = db.Column(db.Integer,db.ForeignKey('client_types.id', onupdate='RESTRICT',
                                                        ondelete='RESTRICT'))
    client_type = db.relationship('ClientTypes', backref=db.backref("clients"))

    financing_id = db.Column(db.Integer, db.ForeignKey('financings.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    financing = db.relationship('Financings', backref=db.backref("clients"))

    agent_id = db.Column(db.Integer, db.ForeignKey('agents.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    agent = db.relationship('Agents', backref=db.backref("agents"))

    def __init__(self, notes, uuid, city_uuid,insert_user_uuid, update_user_uuid,company_uuid, name, last_name, second_last_name,
                 phone_number, email, curp, rfc, gender, birth_date, place_of_birth, purchase_range_initial, purchase_range_final,
                 client_type_id, financing_id, agent_id = agent_id):
        self.notes = notes
        self.uuid = uuid
        self.city_uuid = city_uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid
        self.name = name
        self.last_name = last_name
        self.second_last_name = second_last_name
        self.phone_number = phone_number
        self.email = email
        self.curp = curp
        self.rfc = rfc
        self.gender = gender
        self.birth_date = birth_date
        self.place_of_birth = place_of_birth
        self.purchase_range_initial = purchase_range_initial
        self.purchase_range_final = purchase_range_final
        self.client_type_id = client_type_id
        self.financing_id = financing_id
        self.agent_id = agent_id


class ClientTypes(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    def __init__(self, description, notes, uuid, insert_user_uuid, update_user_uuid, company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Financings(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)
    financing_type_id = db.Column(db.Integer, db.ForeignKey('financing_types.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    financing_type = db.relationship('Financing_types', backref=db.backref("financings"))

    def __init__(self, notes, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid, description, financing_type_id ):
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid
        self.description = description
        self.financing_type_id = financing_type_id


class Financing_types(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    def __init__(self, description, notes, uuid, insert_user_uuid, update_user_uuid, company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Phases(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    def __init__(self, description, notes, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Phase_financing_types(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    phase_id = db.Column(db.Integer, db.ForeignKey('phases.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    phase = db.relationship('Phases', backref=db.backref("financing_phase_types"))

    financing_type_id = db.Column(db.Integer, db.ForeignKey('financing_types.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    financing_type = db.relationship('Financing_types', backref=db.backref("financing_phase_types"))

    UniqueConstraint('phase_id', 'financing_type_id', name='uix_financing_phase_type')


    def __init__(self, description, notes, phase_id, financing_type_id, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.phase_id = phase_id
        self.financing_type_id = financing_type_id
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Document_types(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    def __init__(self, description, notes, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Documents(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    file = db.Column(db.String)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    document_type_id = db.Column(db.Integer,db.ForeignKey('document_types.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    document_type = db.relationship('Document_types', backref=db.backref("document"))

    def __init__(self, description, notes, file, document_type_id, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.file = file
        self.document_type_id = document_type_id
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Client_phase(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    phase_id = db.Column(db.Integer, db.ForeignKey('phases.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    phase = db.relationship('Phases', backref=db.backref("client_phase"))

    client_id = db.Column(db.Integer, db.ForeignKey('clients.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    client = db.relationship('Clients', backref=db.backref("client_phase"))

    UniqueConstraint('phase_id', 'client_id', name='uix_client_phase')

    def __init__(self, description, notes, phase_id, client_id, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.phase_id = phase_id
        self.client_id = client_id
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Phase_document_type(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    phase_id = db.Column(db.Integer, db.ForeignKey('phases.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    phase = db.relationship('Phases', backref=db.backref("phase_document_type"))

    document_type_id = db.Column(db.Integer, db.ForeignKey('document_types.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    document_type = db.relationship('Document_types', backref=db.backref("phase_document_type"))

    UniqueConstraint('phase_id', 'document_type_id', name='uix_phase_document_type')

    def __init__(self, description, notes, phase_id, document_type_id, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.phase_id = phase_id
        self.document_type_id = document_type_id
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Agent_types(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    def __init__(self, description, notes, uuid, insert_user_uuid, update_user_uuid, company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Agents(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    second_last_name = db.Column(db.String(100))
    phone_number = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100))
    curp = db.Column(db.String(100), unique=True)
    rfc = db.Column(db.String(100), unique=True)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    agent_type_id = db.Column(db.Integer,db.ForeignKey('agent_types.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    agent_type = db.relationship('Agent_types', backref=db.backref("agents"))

    def __init__(self, notes, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid, name, last_name, second_last_name, phone_number, email, curp, rfc, agent_type_id):
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid
        self.name = name
        self.last_name = last_name
        self.second_last_name = second_last_name
        self.phone_number = phone_number
        self.email = email
        self.curp = curp
        self.rfc = rfc
        self.agent_type_id = agent_type_id


class Project_types(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    def __init__(self, description, notes, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Projects(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    cp = db.Column(db.String(100))
    street = db.Column(db.String(100))
    colony = db.Column(db.String)
    street_address = db.Column(db.String)
    apartment_number = db.Column(db.String(100))
    city_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    project_type_id = db.Column(db.Integer,db.ForeignKey('project_types.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    project_type = db.relationship('Project_types', backref=db.backref("project"))

    contractor_id = db.Column(db.Integer, db.ForeignKey('contractors.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    contractor = db.relationship('Contractors', backref=db.backref("contractor"))

    def __init__(self, description, notes, cp, street, colony, street_address, apartment_number, project_type_id, contractor_id, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid
        self.cp = cp
        self.street = street
        self.colony = colony
        self.street_address = street_address
        self.apartment_number = apartment_number
        self.project_type_id = project_type_id
        self.contractor_id = contractor_id


class Contractor_types(db.Model):
    uuid_insert = uuid.uuid4()
    user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    def __init__(self, description, notes, uuid, insert_user_uuid, update_user_uuid,
                 company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid


class Contractors(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    second_last_name = db.Column(db.String(100))
    phone_number = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100))
    curp = db.Column(db.String(100), unique=True)
    rfc = db.Column(db.String(100), unique=True)
    cp = db.Column(db.String(100))
    street = db.Column(db.String(100))
    colony = db.Column(db.String)
    street_address = db.Column(db.String)
    apartment_number = db.Column(db.String(100))
    business_name = db.Column(db.String(100))
    legal_entity = db.Column(db.String(100))
    city_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    contractor_type_id = db.Column(db.Integer,db.ForeignKey('contractor_types.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    contractor_type = db.relationship('Contractor_types', backref=db.backref("contractor"))

    def __init__(self, notes, uuid, insert_user_uuid, update_user_uuid, user_uuid,cp, street, colony, street_address, apartment_number,
                 business_name, legal_entity,company_uuid, name, last_name, second_last_name, phone_number, email, curp, rfc, contractor_type_id):
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.user_uuid = user_uuid
        self.company_uuid = company_uuid
        self.name = name
        self.last_name = last_name
        self.second_last_name = second_last_name
        self.phone_number = phone_number
        self.email = email
        self.curp = curp
        self.rfc = rfc
        self.cp = cp
        self.street = street
        self.colony = colony
        self.street_address = street_address
        self.apartment_number = apartment_number
        self.business_name = business_name
        self.legal_entity = legal_entity
        self.contractor_type_id = contractor_type_id


class Locations(db.Model):
    uuid_insert = uuid.uuid4()
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(100), nullable=False, unique=True)
    notes = db.Column(db.String)
    status = db.Column(db.Integer, nullable=False, default=1)
    uuid = db.Column(UUID(as_uuid=True), nullable=False, unique=True)
    insert_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    update_user_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    created_on = db.Column(db.DateTime(), default=datetime.now, nullable=False)
    update_on = db.Column(db.DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)
    cp = db.Column(db.String(100))
    street = db.Column(db.String(100))
    colony = db.Column(db.String)
    street_address = db.Column(db.String)
    apartment_number = db.Column(db.String(100))
    lot = db.Column(db.String)
    block = db.Column(db.String)
    city_uuid = db.Column(UUID(as_uuid=True), nullable=False, default=uuid_insert)
    company_uuid = db.Column(UUID(as_uuid=True), nullable=False)
    consecutive = db.Column(db.Integer)

    project_id = db.Column(db.Integer,db.ForeignKey('projects.id', onupdate='RESTRICT', ondelete='RESTRICT'))
    project = db.relationship('Projects', backref=db.backref("locations"))

    def __init__(self, description, notes, cp, street, colony, street_address, apartment_number, project_id, lot, block, uuid,
                 insert_user_uuid, update_user_uuid, company_uuid):
        self.description = description
        self.notes = notes
        self.uuid = uuid
        self.insert_user_uuid = insert_user_uuid
        self.update_user_uuid = update_user_uuid
        self.company_uuid = company_uuid
        self.cp = cp
        self.street = street
        self.colony = colony
        self.street_address = street_address
        self.apartment_number = apartment_number
        self.lot = lot
        self.block = block
        self.project_id = project_id



class CompanySchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'description',
                  'notes',
                  'status',
                  'city_id',
                  'city',
                  'city_status',
                  'state_id',
                  'state',
                  'state_status',
                  'country_id',
                  'country',
                  'country_status'
                  )


class UrlGroupSchema(ma.Schema):
    class Meta:
        fields = ('url_id',
                  'group_id',
                  'url',
                  'url_status',
                  'group',
                  'group_status',
                  'method'
                  )
        
        
class CountrySchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')


class ApplicationSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status', 'port')


class GroupSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')


class ResourceSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status', 'application_id', 'application', 'application_status')


class StateSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'country', 'notes', 'status', 'country_id', 'country_status')


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'uuid', 'description', 'user', 'group', 'notes', 'status', 'group_id',
                  'group_status', 'name', 'last_name', 'second_last_name', 'password', 'group_uuid',
                  'user_uuid')


class CitySchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'state', 'notes', 'status', 'state_id', 'state_status', 'country',
                  'country_id', 'country_status')


class UrlSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'resource', 'notes', 'status', 'resource_id', 'resource_status',
                  'application', 'application_id', 'application_status', 'method')


class TokenShema(ma.Schema):
    class Meta:
        fields = ('id', 'group_uuid', 'user_uuid', 'token', 'created_on')


#****************** CLIENTS ****************
class LocationSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'description',
                  'notes',
                  'status',
                  'cp',
                  'street',
                  'colony',
                  'street_address',
                  'apartment_number',
                  'lot',
                  'block',
                  'project_id',
                  'project',
                  'project_status')


class projectSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'description',
                  'notes',
                  'status',
                  'cp',
                  'street',
                  'colony',
                  'street_address',
                  'apartment_number',
                  'project_type_id',
                  'project_type',
                  'project_type_status',
                  'contractor_id',
                  'contractor',
                  'contractor_status',
                  'contractor_type_id',
                  'contractor_type',
                  'contractor_type_status')


class ProjectTypeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')


class ContractorSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'name',
                  'last_name',
                  'second_last_name',
                  'phone_number',
                  'email',
                  'curp',
                  'rfc',
                  'notes',
                  'status',
                  'cp',
                  'street',
                  'colony',
                  'street_address',
                  'apartment_number',
                  'business_name',
                  'legal_entity',
                  'contractor_type',
                  'contractor_type_id',
                  'contractor_type_status')


class ContractorTypeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')


class AgentSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'name',
                  'last_name',
                  'second_last_name',
                  'phone_number',
                  'email',
                  'curp',
                  'rfc',
                  'notes',
                  'status',
                  'agent_type',
                  'agent_type_id',
                  'agent_type_status')


class AgentTypeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')


class PhaseDocumentTypeSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'description',
                  'notes',
                  'status',
                  'phase_id',
                  'phase',
                  'phase_status',
                  'document_type_id',
                  'document_type',
                  'document_type_status')


class ClientPhaseSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'description',
                  'notes',
                  'status',
                  'client_id',
                  'client',
                  'client_status',
                  'phase_id',
                  'phase',
                  'phase_status')


class DocumentSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'description',
                  'notes',
                  'status',
                  'file',
                  'document_type_id',
                  'document_type',
                  'document_type_status')


class DocumentTypeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')


class PhaseFinancingTypeSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'description',
                  'notes',
                  'status',
                  'phase_id',
                  'phase',
                  'phase_status',
                  'financing_type_id',
                  'financing_type',
                  'financing_type_status')


class ClientSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'name',
                  'last_name',
                  'second_last_name',
                  'phone_number',
                  'email',
                  'curp',
                  'rfc',
                  'gender',
                  'birth_date',
                  'place_of_birth',
                  'purchase_range_initial',
                  'purchase_range_final',
                  'notes',
                  'status',
                  'client_type',
                  'client_type_id',
                  'client_type_status',
                  'financing',
                  'financing_id',
                  'financing_status',
                  'financing_type',
                  'financing_type_id',
                  'financing_type_status',
                  'agent',
                  'agent_id',
                  'agent_status')


class ClientTypeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')


class FinancingSchema(ma.Schema):
    class Meta:
        fields = ('id',
                  'description',
                  'notes',
                  'status',
                  'financing_type',
                  'financing_type_id',
                  'financing_type_status')


class FinancingTypeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')


class PhaseSchema(ma.Schema):
    class Meta:
        fields = ('id', 'description', 'notes', 'status')