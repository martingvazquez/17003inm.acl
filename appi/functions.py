import uuid
from datetime import datetime, timedelta


def get_states(db, models):
    states = db.session.query(models.States.id, models.States.status, models.States.country_id,
                              models.States.description, models.States.notes,
                              models.Countries.description.label('country'),
                              models.Countries.id.label('country_id'),
                              models.Countries.status.label('country_status'))
    states = states.join(models.Countries)
    return states


def get_users(db, models):

    users = db.session.query(models.Users.id, models.Users.status, models.Users.description,
                             models.Users.notes, models.Users.user, models.Users.password,
                             models.Users.name, models.Users.second_last_name, models.Users.last_name,
                             models.Users.uuid, models.Groups.description.label('group'),
                             models.Groups.id.label('group_id'),
                             models.Groups.status.label('group_status'),
                             models.Groups.uuid.label('group_uuid'),
                             models.Users.uuid.label('user_uuid'))
    users = users.join(models.Groups)
    return users


def get_resources(db, models):
    resources = db.session.query(models.Resources.id, models.Resources.status, 
                                 models.Resources.application_id, models.Resources.description,
                                 models.Resources.notes,
                                 models.Applications.description.label('application'),
                                 models.Applications.id.label('application_id'),
                                 models.Applications.status.label('application_status'))
    resources = resources.join(models.Applications)
    return resources


def get_groups(db, models):
    groups = db.session.query(models.Groups.id,
                              models.Groups.description,
                              models.Groups.notes,
                              models.Groups.status)
    return groups


def get_cities(db, models):
    cities = db.session.query(models.Cities.id,
                              models.Cities.description,
                              models.Cities.notes,
                              models.Cities.status,
                              models.Cities.state_id,
                              models.States.description.label('state'),
                              models.States.id.label('state_id'),
                              models.States.status.label('state_status'),
                              models.Countries.description.label('country'),
                              models.Countries.id.label('country_id'),
                              models.Countries.status.label('country_status')
                              )
    cities = cities.join(models.States)
    cities = cities.join(models.Countries)
    return cities

def get_urls(db, models):
    urls = db.session.query(models.Urls.id, models.Urls.description, models.Urls.notes,
                            models.Urls.status, models.Urls.resource_id,
                            models.Resources.description.label('resource'),
                            models.Resources.id.label('resource_id'),
                            models.Resources.status.label('resource_status'),
                            models.Applications.description.label('application'),
                            models.Applications.id.label('application_id'),
                            models.Applications.status.label('application_status'),
                            models.Urls.method.label('method'))
    urls = urls.join(models.Resources)
    urls = urls.join(models.Applications)
    return urls

def get_companies(db, models):
    try:
        companies = db.session.query(models.Companies.id, models.Companies.description,
                                     models.Companies.notes,
                                     models.Companies.status, models.Cities.id.label('city_id'),
                                     models.Cities.description.label('city'),
                                     models.Cities.status.label('city_status'),
                                     models.States.id.label('state_id'),
                                     models.States.description.label('state'),
                                     models.States.status.label('state_status'),
                                     models.Countries.id.label('country_id'),
                                     models.Countries.description.label('country'),
                                     models.Countries.status.label('country_status'))
        companies = companies.join(models.Cities)
        companies = companies.join(models.States)
        companies = companies.join(models.Countries)
        return companies
    except Exception as e:
        print(e)
        return e


def get_order_by(column_order_by, type_order):
    if column_order_by == '' or column_order_by == '-1':
        order = ''
    else:
        order = column_order_by + ' ' + type_order

    return order


def get_data_filter(data_filter):
    if data_filter == '' or data_filter == '-1':
        data_filter_integer = '0'
        data_filter_string = ''
    else:
        try:
            int(data_filter)
            data_filter_integer = data_filter
        except ValueError:
            data_filter_integer = '0'

        data_filter_string = data_filter.replace("|", " ")

    return data_filter_integer, data_filter_string


def review_token(db, models):
    tokens = get_tokens(db, models)
    tokens_schema = models.TokenShema(many=True)
    tokens = tokens_schema.dump(tokens).data
    now = datetime.now()

    for token in tokens:
        created_on = token['created_on']
        token_id = token['id']
        created_split = created_on.split('T')
        date = created_split[0]
        hour = created_split[1]
        date_split = date.split('-')
        year = int(date_split[0])
        mes = int(date_split[1])
        dia = int(date_split[2])
        hour_split = hour.split('.')
        complete_hour = hour_split[0]
        complete_hour_split = complete_hour.split(':')
        hour = int(complete_hour_split[0])
        minutes = int(complete_hour_split[1])
        seconds = int(complete_hour_split[2])
        create = datetime(year, mes, dia, hour, minutes, seconds)
        current_time = datetime(now.year, now.month, now.day, now.hour, now.minute, now.second)
        finished = create + timedelta(seconds=300)
        if finished <= current_time:
            query = db.session.query(models.Tokens)
            query = query.filter(models.Tokens.id == token_id)
            tokens_delete = query.one()
            db.session.delete(tokens_delete)
            db.session.commit()

def access(db, models, url, method, token):

    review_token(db, models)
    urls = get_urls(db, models)
    urls = urls.filter(models.Urls.description == url)
    urls = urls.filter(models.Urls.method == method)

    n_urls = urls.count()

    if n_urls == 0:
        return -1

    urls = urls.all()
    url_id = urls[0].id
    tokens = get_tokens(db, models)
    tokens = tokens.filter(models.Tokens.token == token)
    n_tokens = tokens.count()

    if n_tokens == 0:
        return -1
    else:
        tokens_schema = models.TokenShema(many=True)
        tokens = tokens_schema.dump(tokens).data
        group_uuid = tokens[0]['group_uuid']
        groups = get_groups(db, models)
        groups = groups.filter(models.Groups.uuid == group_uuid)
        groups = groups.all()
        group_id = groups[0].id
        
        permissions = db.session.query(models.UrlsGroups)
        permissions = permissions.filter(models.UrlsGroups.url_id == url_id)
        permissions = permissions.filter(models.UrlsGroups.group_id == group_id)
        n_permissions = permissions.count()

        if n_permissions == 0:
            return -1

        tokens = db.session.query(models.Tokens)
        tokens = tokens.filter(models.Tokens.token == token)
        tokens.update({'created_on': datetime.now()})
        db.session.commit()
        return 1


def access_external_application(db, models, application, resource, method, token):
    review_token(db, models)
    url = '/{}/{}'.format(application, resource)
    urls = get_urls(db, models)
    urls = urls.filter(models.Urls.description == url)
    urls = urls.filter(models.Urls.method == method)

    n_urls = urls.count()

    if n_urls == 0:
        return -1

    urls = urls.all()
    url_id = urls[0].id
    tokens = get_tokens(db, models)
    tokens = tokens.filter(models.Tokens.token == token)
    n_tokens = tokens.count()

    if n_tokens == 0:
        return -1
    else:
        tokens_schema = models.TokenShema(many=True)
        tokens = tokens_schema.dump(tokens).data
        group_uuid = tokens[0]['group_uuid']
        groups = get_groups(db, models)
        groups = groups.filter(models.Groups.uuid == group_uuid)
        groups = groups.all()
        group_id = groups[0].id

        permissions = db.session.query(models.UrlsGroups)
        permissions = permissions.filter(models.UrlsGroups.url_id == url_id)
        permissions = permissions.filter(models.UrlsGroups.group_id == group_id)
        n_permissions = permissions.count()

        if n_permissions == 0:
            return -1

        tokens = db.session.query(models.Tokens)
        tokens = tokens.filter(models.Tokens.token == token)
        tokens.update({'created_on': datetime.now()})
        db.session.commit()
        return 1


def get_tokens(db, models):
    tokens = db.session.query(models.Tokens.group_uuid, models.Tokens.user_uuid, models.Tokens.token,
                              models.Tokens.created_on, models.Tokens.id)
    return tokens

def get_agents(db, models):
    agents = db.session.query(models.Agents.id, models.Agents.name, models.Agents.last_name,
                              models.Agents.second_last_name, models.Agents.phone_number, models.Agents.email,
                              models.Agents.curp, models.Agents.rfc, models.Agents.notes,
                              models.Agents.status, models.Agent_types.description.label('agent_type'),
                              models.Agent_types.id.label('agent_type_id'),
                              models.Agent_types.status.label('agent_type_status'))
    agents = agents.join(models.Agent_types)
    return agents

def get_clients(db, models):
    clients = db.session.query(models.Clients.id, models.Clients.name, models.Clients.last_name,
                               models.Clients.second_last_name, models.Clients.phone_number,
                               models.Clients.email, models.Clients.curp, models.Clients.rfc,
                               models.Clients.notes, models.Clients.status, models.Clients.gender,
                               models.Clients.birth_date, models.Clients.place_of_birth,
                               models.Clients.purchase_range_initial, models.Clients.purchase_range_final,
                               models.ClientTypes.description.label('client_type'),
                               models.ClientTypes.id.label('client_type_id'),
                               models.ClientTypes.status.label('client_type_status'),
                               models.Financings.description.label('financing'),
                               models.Financings.id.label('financing_id'),
                               models.Financings.status.label('financing_status'),
                               models.Financing_types.description.label('financing_type'),
                               models.Financing_types.id.label('financing_type_id'),
                               models.Financing_types.status.label('financing_type_status'),
                               models.Agents.name.label('agent'), models.Agents.id.label('agent_id'),
                               models.Agents.status.label('agent_status')
                               )
    clients = clients.join(models.ClientTypes)
    clients = clients.join(models.Financings)
    clients = clients.join(models.Financing_types)
    clients = clients.join(models.Agents)
    return clients

def get_contractors(db, models):
    contractors = db.session.query(models.Contractors.id, models.Contractors.name,
                                   models.Contractors.last_name, models.Contractors.second_last_name,
                                   models.Contractors.phone_number, models.Contractors.email,
                                   models.Contractors.curp, models.Contractors.rfc,
                                   models.Contractors.notes,
                                   models.Contractors.status,
                                   models.Contractors.cp,
                                   models.Contractors.street,
                                   models.Contractors.colony,
                                   models.Contractors.street_address,
                                   models.Contractors.apartment_number,
                                   models.Contractors.business_name,
                                   models.Contractors.legal_entity,
                                   models.Contractor_types.description.label('contractor_type'),
                                   models.Contractor_types.id.label('contractor_type_id'),
                                   models.Contractor_types.status.label('contractor_type_status') )
    contractors = contractors.join(models.Contractor_types)
    return contractors


def get_financings(db, models):
    financings = db.session.query(models.Financings.id,
                                  models.Financings.description,
                                  models.Financings.notes,
                                  models.Financings.status,
                                  models.Financing_types.description.label('financing_type'),
                                  models.Financing_types.id.label('financing_type_id'),
                                  models.Financing_types.id.label('financing_type_status') )
    financings = financings.join(models.Financing_types)
    return financings


def get_phase_financing_types(db, models):
    financingPhaseTypes = db.session.query(models.Phase_financing_types.id,
                                           models.Phase_financing_types.description,
                                           models.Phase_financing_types.status,
                                           models.Phases.description.label('phase'),
                                           models.Phases.id.label('phase_id'),
                                           models.Phases.status.label('phase_status'),
                                           models.Financing_types.description.label('financing_type'),
                                           models.Financing_types.id.label('financing_type_id'),
                                           models.Financing_types.id.label('financing_type_status'))

    financingPhaseTypes = financingPhaseTypes.join(models.Phases)
    financingPhaseTypes = financingPhaseTypes.join(models.Financing_types)
    return financingPhaseTypes


def get_documents(db, models):
    documents = db.session.query(models.Documents.id,
                                 models.Documents.description,
                                 models.Documents.notes,
                                 models.Documents.status,
                                 models.Documents.file,
                                 models.Document_types.description.label('document_type'),
                                 models.Document_types.id.label('document_type_id'),
                                 models.Document_types.status.label('document_type_status') )

    documents = documents.join(models.Document_types)
    return documents


def get_projects(db, models):
    projects = db.session.query(models.Projects.id,
                                models.Projects.description,
                                models.Projects.notes,
                                models.Projects.status,
                                models.Projects.cp,
                                models.Projects.street,
                                models.Projects.colony,
                                models.Projects.street_address,
                                models.Projects.apartment_number,
                                models.Project_types.description.label('project_type'),
                                models.Project_types.id.label('project_type_id'),
                                models.Project_types.status.label('project_type_status'),
                                models.Contractors.name.label('contractor'),
                                models.Contractors.id.label('contractor_id'),
                                models.Contractors.status.label('contractor_status'),
                                models.Contractors.name.label('contractor_type'),
                                models.Contractors.id.label('contractor_type_id'),
                                models.Contractors.status.label('contractor_type_status'))

    projects = projects.join(models.Project_types)
    projects = projects.join(models.Contractors)
    return projects


def get_locations(db, models):
    locations = db.session.query(models.Locations.id, models.Locations.description, models.Locations.notes,
                                 models.Locations.status, models.Locations.cp, models.Locations.street,
                                 models.Locations.colony, models.Locations.street_address,
                                 models.Locations.apartment_number, models.Locations.lot,
                                 models.Locations.block, models.Projects.description.label('project'),
                                 models.Projects.id.label('project_id'),
                                 models.Projects.status.label('project_status'))

    locations = locations.join(models.Projects)
    return locations

