from common import Login, Operations
from random import randint
import unittest
import requests
from time import time


class TestGetResources(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):

        #Eliminar Registros
        #Crear de paginacion y rendimiento

        pass

    def test_get_application(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/applications/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("applications", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()

    def test_get_city(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/cities/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("cities", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()

    def test_get_company(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/companies/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("companies", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()

    def test_get_country(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/countries/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("countries", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()

    def test_get_group(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/groups/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("groups", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()

    def test_get_resource(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/resources/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("resources", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()

    def test_get_state(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/states/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("states", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()

    def test_get_url(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/urls/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("urls", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()

    def test_get_user(self):
        login = self.login.login()
        response = requests.get("http://localhost/acl/users/token/{}".format(login["token"]))
        data = response.json()
        n_data = int(data['n_rows'])
        self.assertIn("n_rows", data)
        self.assertIn("users", data)
        self.assertGreaterEqual(n_data, 0)
        self.login.logout()



if __name__ == '__main__':
    unittest.main()