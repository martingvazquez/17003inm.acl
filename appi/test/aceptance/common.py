import requests
from random import randint


class Login():

    def __init__(self, *args, **kwargs):
        self.token = None
        self.base_url = "http://localhost/"

    def login(self, user='root', password='moro58'):
        requests.delete('{}/acl/sessions/{}'.format(self.base_url, 'moro58'))
        r = requests.get('{}acl/sessions/valid/{}/{}'.format(self.base_url, user, password))

        self.token = r.json()
        return self.token

    def logout(self):
        r = requests.delete('{}/acl/sessions/{}'.format(self.base_url, 'moro58'))
        return r.json()

class Operations():

    def __init__(self, resource, token):
        self.total_items = None
        self.id_ini = None
        self.id_fin = None
        self.id_exist = False

        self.resource = resource
        self.token = token

        self.get_total_items()
        self.get_ini_item()
        self.get_fin_item()

    def get_total_items(self):
        response = requests.get("http://localhost/acl/{}/token/{}".format(self.resource, self.token))
        data = response.json()
        self.total_items = data['n_rows']

    def get_ini_item(self):
        response = requests.get("http://localhost/acl/{}/token/{}/{}.id/"
                                "ASC/-1/1/1"
                                .format(self.resource, self.token, self.resource))
        data = response.json()
        self.id_ini = data[self.resource][0]['id']

    def get_fin_item(self):
        response = requests.get("http://localhost/acl/{}/token/{}/{}.id/DESC/-1/1/1"
                                .format(self.resource, self.token, self.resource))
        data = response.json()
        self.id_fin = data[self.resource][0]['id']

    def valid_exist(self):
        resource_id = randint(self.id_ini, self.id_fin)
        response = requests.get("http://localhost/acl/{}/token/{}/{}"
                                .format(self.resource, self.token, str(resource_id)))
        data = response.json()
        n_rows = data['n_rows']
        if n_rows == 0:
            return -1
        else:
            return resource_id

