from common import Login, Operations
from random import randint
import unittest
import requests
from time import time


class TestAddAllNotColumnRequired(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):

        #Eliminar Registros
        #Crear de paginacion y rendimiento

        pass

    def test_add_application_not_column_required(self):
        login = self.login.login()
        for i in range(5000, 5001):
            payload = {"notes": str(time())}
            response = requests.post("http://localhost/acl/applications/token/{}".format(login["token"]),
                                     data=payload)
            data = response.json()
            self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_add_city_not_column_required(self):
        login = self.login.login()
        payload = {"notes": str(time())}
        response = requests.post("http://localhost/acl/cities/token/{}".format(login["token"]),
                                 data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_add_company_not_column_required(self):
        login = self.login.login()
        payload = {"notes": str(time())}
        response = requests.post("http://localhost/acl/companies/token/{}".format(login["token"]),
                                 data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_add_country_not_column_required(self):
        login = self.login.login()
        payload = {"notes": str(time())}
        response = requests.post("http://localhost/acl/countries/token/{}".format(login["token"]),
                                 data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_add_group_not_column_required(self):
        login = self.login.login()
        payload = {"notes": str(time())}
        response = requests.post("http://localhost/acl/groups/token/{}".format(login["token"]),
                                 data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_add_resource_not_column_required(self):
        login = self.login.login()
        payload = {"notes": str(time())}
        response = requests.post("http://localhost/acl/resources/token/{}".format(login["token"]),
                                 data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_add_state_not_column_required(self):
        login = self.login.login()
        payload = {"notes": str(time())}
        response = requests.post("http://localhost/acl/states/token/{}".format(login["token"]),
                                 data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_add_url_not_column_required(self):
        login = self.login.login()
        payload = {"notes": str(time())}
        response = requests.post("http://localhost/acl/urls/token/{}".format(login["token"]),
                                 data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_add_user_not_column_required(self):
        login = self.login.login()
        payload = {"notes": str(time())}
        response = requests.post("http://localhost/acl/users/token/{}".format(login["token"]),
                                 data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()



if __name__ == '__main__':
    unittest.main()