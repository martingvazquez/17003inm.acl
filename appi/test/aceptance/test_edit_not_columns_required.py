from common import Login, Operations
from random import randint
import unittest
import requests
from time import time


class TestEditAllNotColumnRequired(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):

        #Eliminar Registros
        #Crear de paginacion y rendimiento

        pass

    def test_edit_application_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='applications', token=login['token'])
        for i in range(5000, 5001):
            application_id = operations.valid_exist()
            payload = {"notes": str(time())}
            response = requests.put("http://localhost/acl/applications/token/{}/{}".format(login["token"],
                                                                                           application_id),
                                    data=payload)
            data = response.json()
            self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_edit_city_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='cities', token=login['token'])
        city_id = operations.valid_exist()
        payload = {"notes": str(time())}
        response = requests.put("http://localhost/acl/cities/token/{}/{}".format(login["token"], city_id),
                                data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_edit_company_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='companies', token=login['token'])
        company_id = operations.valid_exist()
        payload = {"notes": str(time())}
        response = requests.put("http://localhost/acl/companies/token/{}/{}".format(login["token"], company_id),
                                data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_edit_country_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='countries', token=login['token'])
        country_id = operations.valid_exist()
        payload = {"notes": str(time())}
        response = requests.put("http://localhost/acl/countries/token/{}/{}".format(login["token"], country_id),
                                data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_edit_group_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='groups', token=login['token'])
        group_id = operations.valid_exist()
        payload = {"notes": str(time())}
        response = requests.put("http://localhost/acl/groups/token/{}/{}".format(login["token"], group_id),
                                data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_edit_resource_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='resources', token=login['token'])
        resource_id = operations.valid_exist()
        payload = {"notes": str(time())}
        response = requests.put("http://localhost/acl/resources/token/{}/{}".format(login["token"], resource_id),
                                data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_edit_state_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='states', token=login['token'])
        state_id = operations.valid_exist()
        payload = {"notes": str(time())}
        response = requests.put("http://localhost/acl/states/token/{}/{}".format(login["token"], state_id),
                                data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()

    def test_edit_url_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='urls', token=login['token'])
        url_id = operations.valid_exist()
        if url_id > 0:
            payload = {"notes": str(time())}
            response = requests.put("http://localhost/acl/urls/token/{}/{}".format(login["token"], url_id),
                                    data=payload)
            data = response.json()
            self.assertEqual(data["error"], "406")
            self.login.logout()

    def test_edit_user_not_column_required(self):
        login = self.login.login()
        operations = Operations(resource='users', token=login['token'])
        user_id = operations.valid_exist()
        payload = {"notes": str(time())}
        response = requests.put("http://localhost/acl/users/token/{}/{}".format(login["token"], user_id),
                                data=payload)
        data = response.json()
        self.assertEqual(data["error"], "406")
        self.login.logout()


if __name__ == '__main__':
    unittest.main()