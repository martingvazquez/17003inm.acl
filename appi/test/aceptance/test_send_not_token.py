from common import Login
import unittest
import requests
from time import time


class NotTokenTestGet(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):
        pass

    def test_applications_get(self):
        response = requests.get("http://localhost/acl/applications/token/xxx")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_cities_get(self):
        response = requests.get("http://localhost/acl/cities/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_companies_get(self):
        response = requests.get("http://localhost/acl/companies/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_countries_get(self):
        response = requests.get("http://localhost/acl/countries/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_groups_get(self):
        response = requests.get("http://localhost/acl/groups/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_groups_urls_get(self):
        response = requests.get("http://localhost/acl/urls/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_resources_get(self):
        response = requests.get("http://localhost/acl/resources/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_states_get(self):
        response = requests.get("http://localhost/acl/states/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_urls_get(self):
        response = requests.get("http://localhost/acl/urls/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_users_get(self):
        response = requests.get("http://localhost/acl/users/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")


class NotTokenTestPost(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):
        pass

    def test_applications_post(self):
        response = requests.post("http://localhost/acl/applications/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_cities_post(self):
        response = requests.post("http://localhost/acl/cities/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_companies_post(self):
        response = requests.post("http://localhost/acl/companies/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_countries_post(self):
        response = requests.post("http://localhost/acl/countries/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_groups_post(self):
        response = requests.post("http://localhost/acl/groups/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_groups_urls_post(self):
        response = requests.post("http://localhost/acl/urls/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_resources_post(self):
        response = requests.post("http://localhost/acl/resources/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_states_post(self):
        response = requests.post("http://localhost/acl/states/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_urls_post(self):
        response = requests.post("http://localhost/acl/urls/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_users_post(self):
        response = requests.post("http://localhost/acl/users/token/1")
        data = response.json()
        self.assertEqual(data["error"], "401")


class NotTokenTestPut(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):
        pass

    def test_applications_put(self):
        response = requests.put("http://localhost/acl/applications/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_cities_put(self):
        response = requests.put("http://localhost/acl/cities/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_companies_put(self):
        response = requests.put("http://localhost/acl/companies/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_countries_put(self):
        response = requests.put("http://localhost/acl/countries/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_groups_put(self):
        response = requests.put("http://localhost/acl/groups/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_groups_urls_put(self):
        response = requests.put("http://localhost/acl/urls/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_resources_put(self):
        response = requests.put("http://localhost/acl/resources/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_states_put(self):
        response = requests.put("http://localhost/acl/states/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_urls_put(self):
        response = requests.put("http://localhost/acl/urls/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_users_put(self):
        response = requests.put("http://localhost/acl/users/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")


class NotTokenTestPatch(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):
        pass

    def test_applications_patch(self):
        response = requests.patch("http://localhost/acl/applications/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_cities_patch(self):
        response = requests.patch("http://localhost/acl/cities/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_companies_patch(self):
        response = requests.patch("http://localhost/acl/companies/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_countries_patch(self):
        response = requests.patch("http://localhost/acl/countries/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_groups_patch(self):
        response = requests.patch("http://localhost/acl/groups/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_groups_urls_patch(self):
        response = requests.patch("http://localhost/acl/urls/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_resources_patch(self):
        response = requests.patch("http://localhost/acl/resources/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_states_patch(self):
        response = requests.patch("http://localhost/acl/states/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_urls_patch(self):
        response = requests.patch("http://localhost/acl/urls/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")

    def test_users_patch(self):
        response = requests.patch("http://localhost/acl/users/token/1/1")
        data = response.json()
        self.assertEqual(data["error"], "401")


if __name__ == '__main__':
    unittest.main()