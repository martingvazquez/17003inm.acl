from common import Login, Operations
from random import randint
import unittest
import requests
from time import time


class TestAddAll(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):

        #Eliminar Registros
        #Crear de paginacion y rendimiento

        pass

    def test_add_application(self):
        login = self.login.login()
        for i in range(5000, 5100):
            payload = {"description": str(time()), "notes": str(time()), "port": str(i)}
            response = requests.post("http://localhost/acl/applications/token/{}".format(login["token"]),
                                     data=payload)
            data = response.json()
            self.assertEqual(data["status"], "ok")
        self.login.logout()

    def test_add_cities(self):
        login = self.login.login()
        i = 0
        operations = Operations(resource='states', token=login['token'])
        total_items = operations.total_items
        while i < 2:
            state_id = operations.valid_exist()
            if state_id > 0:
                payload = {"description": str(time()), "notes": str(time()), "state_id": str(state_id)}
                response = requests.post("http://localhost/acl/cities/token/{}".format(login["token"]),
                                         data=payload)
                data = response.json()
                self.assertEqual(data["status"], "ok")
                i = i + 1
        self.login.logout()

    def test_add_companies(self):
        login = self.login.login()
        i = 0
        operations = Operations(resource='cities', token=login['token'])
        total_items = operations.total_items
        while i < 2:
            city_id = operations.valid_exist()
            if city_id > 0:
                payload = {"description": str(time()), "notes": str(time()), "city_id": str(city_id)}
                response = requests.post("http://localhost/acl/companies/token/{}".format(login["token"]),
                                         data=payload)
                data = response.json()
                self.assertEqual(data["status"], "ok")
                i = i + 1
        self.login.logout()

    def test_add_countries(self):
        login = self.login.login()
        for i in range(1, 2):
            payload = {"description": str(time()), "notes": str(time())}
            response = requests.post("http://localhost/acl/countries/token/{}".format(login["token"]),
                                     data=payload)
            data = response.json()
            self.assertEqual(data["status"], "ok")
        self.login.logout()

    def test_add_groups(self):
        login = self.login.login()
        for i in range(1, 2):
            payload = {"description": str(time()), "notes": str(time())}
            response = requests.post("http://localhost/acl/groups/token/{}".format(login["token"]),
                                     data=payload)
            data = response.json()
            self.assertEqual(data["status"], "ok")
        self.login.logout()

    def test_add_resources(self):
        login = self.login.login()
        i = 0
        operations = Operations(resource='applications', token=login['token'])
        total_items = operations.total_items

        while i < 2:
            application_id = operations.valid_exist()
            if application_id > 0:

                payload = {"description": str(time()), "notes": str(time()),
                           "application_id": str(application_id)}
                response = requests.post("http://localhost/acl/resources/token/{}".format(login["token"]),
                                         data=payload)
                data = response.json()
                self.assertEqual(data["status"], "ok")
                i = i + 1
        self.login.logout()

    def test_add_states(self):
        login = self.login.login()
        i = 0
        operations = Operations(resource='countries', token=login['token'])
        total_items = operations.total_items

        while i < 2:
            country_id = operations.valid_exist()
            if country_id > 0:
                payload = {"description": str(time()), "notes": str(time()), "country_id": str(country_id)}
                response = requests.post("http://localhost/acl/states/token/{}".format(login["token"]),
                                         data=payload)
                data = response.json()
                self.assertEqual(data["status"], "ok")
                i = i + 1
        self.login.logout()

    def test_add_urls(self):
        login = self.login.login()
        i = 0
        operations = Operations(resource='resources', token=login['token'])
        total_items = operations.total_items

        while i < 2:
            resource_id = operations.valid_exist()
            if resource_id > 0:
                payload = {"description": str(time()), "method": str(time()),
                           "notes": str(time()), "resource_id": str(resource_id)}
                response = requests.post("http://localhost/acl/urls/token/{}".format(login["token"]),
                                         data=payload)
                data = response.json()
                self.assertEqual(data["status"], "ok")
                i = i + 1

        self.login.logout()


    def test_add_users(self):
        login = self.login.login()
        i = 0
        operations = Operations(resource='groups', token=login['token'])
        total_items = operations.total_items

        while i < 1:
            group_id = operations.valid_exist()
            if group_id > 0:

                payload = {"user": str(time()), "password": str(time()), "name": str(time()),
                           "last_name": str(time()), "second_last_name": str(time()),
                           "description": str(time()), "notes": str(time()), "group_id": group_id}
                response = requests.post("http://localhost/acl/users/token/{}".format(login["token"]),
                                         data=payload)
                data = response.json()

                self.assertEqual(data["status"], "ok")
                i = i + 1

        self.login.logout()



if __name__ == '__main__':
    unittest.main()