from common import Login, Operations
from random import randint
import unittest
import requests
from time import time


class TestEditAll(unittest.TestCase):

    def setUp(self):
        self.login = Login()

    def tearDown(self):
        #Eliminar Registros
        #Crear de paginacion y rendimiento
        pass

    def test_edit_application(self):
        login = self.login.login()
        operations = Operations(resource='applications', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            application_id = operations.valid_exist()
            if application_id > 1:
                description_change = 'Descripcion'+str(time())
                payload = {"description": description_change,
                           "port": "5001",
                           "notes": str(time())}
                response = requests.put("http://localhost/acl/applications/token/{}/{}"
                                        .format(login["token"], str(application_id)),
                                        data=payload)
                data = response.json()
                self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()

    def test_edit_citie(self):
        login = self.login.login()
        operations = Operations(resource='cities', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            city_id = operations.valid_exist()
            if city_id > 0:
                description_change = 'Descripcion'+str(time())
                operation_foreign = Operations(resource='states', token=login['token'])
                state_id = operation_foreign.valid_exist()
                if state_id > 0:
                    payload = {"description": description_change,
                               "notes": str(time()),
                               "state_id": str(state_id)}
                    response = requests.put("http://localhost/acl/cities/token/{}/{}"
                                            .format(login["token"], str(city_id)),
                                            data=payload)
                    data = response.json()
                    self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()


    def test_edit_company(self):
        login = self.login.login()
        operations = Operations(resource='companies', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            company_id = operations.valid_exist()
            if company_id > 0:
                description_change = 'Descripcion'+str(time())
                operation_foreign = Operations(resource='cities', token=login['token'])
                city_id = operation_foreign.valid_exist()
                if city_id > 0:
                    payload = {"description": description_change,
                               "notes": str(time()),
                               "city_id": str(city_id)}
                    response = requests.put("http://localhost/acl/companies/token/{}/{}"
                                            .format(login["token"], str(company_id)),
                                            data=payload)
                    data = response.json()
                    self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()


    def test_edit_country(self):
        login = self.login.login()
        operations = Operations(resource='countries', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            country_id = operations.valid_exist()
            if country_id > 0:
                description_change = 'Descripcion'+str(time())
                payload = {"description": description_change,
                           "notes": str(time())}
                response = requests.put("http://localhost/acl/countries/token/{}/{}"
                                        .format(login["token"], str(country_id)),
                                        data=payload)
                data = response.json()
                self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()

    def test_edit_group(self):
        login = self.login.login()
        operations = Operations(resource='groups', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            group_id = operations.valid_exist()
            if group_id > 1:
                description_change = 'Descripcion'+str(time())
                payload = {"description": description_change,
                "notes": str(time())}
                response = requests.put("http://localhost/acl/groups/token/{}/{}"
                                        .format(login["token"], str(group_id)),
                                        data=payload)
                data = response.json()
                self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()


    def test_edit_resource(self):
        login = self.login.login()
        operations = Operations(resource='resources', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            resource_id = operations.valid_exist()
            if resource_id > 11:
                description_change = 'Descripcion'+str(time())
                operation_foreign = Operations(resource='applications', token=login['token'])
                application_id = operation_foreign.valid_exist()
                if application_id > 1:
                    payload = {"description": description_change,
                               "notes": str(time()),
                               "application_id": str(application_id)}
                    response = requests.put("http://localhost/acl/resources/token/{}/{}"
                                            .format(login["token"], str(resource_id)),
                                            data=payload)
                    data = response.json()
                    self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()


    def test_edit_state(self):
        login = self.login.login()
        operations = Operations(resource='states', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            state_id = operations.valid_exist()
            if state_id > 0:
                description_change = 'Descripcion'+str(time())
                operation_foreign = Operations(resource='countries', token=login['token'])
                country_id = operation_foreign.valid_exist()
                if country_id > 0:
                    payload = {"description": description_change,
                               "notes": str(time()),
                               "country_id": str(country_id)}
                    response = requests.put("http://localhost/acl/states/token/{}/{}"
                                            .format(login["token"], str(state_id)),
                                            data=payload)
                    data = response.json()
                    self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()


    def test_edit_url(self):
        login = self.login.login()
        operations = Operations(resource='urls', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            url_id = operations.valid_exist()
            if url_id > 0:
                description_change = 'Descripcion'+str(time())
                operation_foreign = Operations(resource='resources', token=login['token'])
                resource_id = operation_foreign.valid_exist()
                if resource_id > 50:
                    payload = {"description": description_change,
                               "notes": str(time()),
                               "resource_id": str(resource_id),
                               "method": str(time())}
                    response = requests.put("http://localhost/acl/urls/token/{}/{}"
                                            .format(login["token"], str(url_id)),
                                            data=payload)
                    data = response.json()
                    self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()


    def test_edit_user(self):
        login = self.login.login()
        operations = Operations(resource='users', token=login['token'])
        total_items = operations.total_items
        i = 0
        while i < 2:
            user_id = operations.valid_exist()
            if user_id > 1:
                description_change = 'Descripcion'+str(time())
                operation_foreign = Operations(resource='groups', token=login['token'])
                group_id = operation_foreign.valid_exist()
                if group_id > 1:
                    payload = {"description": description_change,
                               "notes": str(time()),
                               "group_id": str(group_id),
                               "user": str(time()),
                               "last_name": str(time()),
                               "second_last_name": str(time()),
                               "password": str(time()),
                               "name": str(time())
                               }
                    response = requests.put("http://localhost/acl/users/token/{}/{}"
                                            .format(login["token"], str(user_id)),
                                            data=payload)
                    data = response.json()
                    self.assertEqual(data["status"], "ok")
            i = i +1
        self.login.logout()



if __name__ == '__main__':
    unittest.main()
