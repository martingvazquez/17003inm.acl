import application
from models import models
import uuid

db = application.db
ap = application.app

applications_insert = []

applications_insert = [{'description': 'ACL', 'notes': '', 'port': '5000'},
                       {'description': 'INMOBILIARIA', 'notes': '', 'port': '5001'}]

for application_insert in applications_insert:
    description = application_insert['description']
    notes = application_insert['notes']
    port = application_insert['port']

    applications = db.session.query(models.Applications)
    applications = applications.filter(models.Applications.description == description)
    n_applications = applications.count()
    applications = applications.first()

    if n_applications == 0:
        applications = models.Applications(description=description, notes=notes, uuid=uuid.uuid4(),
                                           port=port)
        try:
            db.session.add(applications)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            print(e)



applications = db.session.query(models.Applications.id, models.Applications.description).all()
for application in applications:
    application_description = application.description
    if application_description == 'INMOBILIARIA':
        resources_insert = []
        resources_insert = [{'description': 'AGENT_TYPES', 'notes': ''},
                            {'description': 'AGENTS', 'notes': ''},
                            {'description': 'CLIENTS', 'notes': ''},
                            {'description': 'CLIENT_TYPES', 'notes': ''},
                            {'description': 'CONTRACTOR_TYPES', 'notes': ''},
                            {'description': 'CONTRACTORS', 'notes': ''},
                            {'description': 'DOCUMENT_TYPES', 'notes': ''},
                            {'description': 'DOCUMENTS', 'notes': ''},
                            {'description': 'FINANCING_TYPES', 'notes': ''},
                            {'description': 'LOCATIONS', 'notes': ''},
                            {'description': 'PHASE_FINANCING_TYPES', 'notes': ''},
                            {'description': 'PHASES', 'notes': ''},
                            {'description': 'PROJECT_TYPES', 'notes': ''},
                            {'description': 'PROJECTS', 'notes': ''},
                            {'description': 'FINANCINGS', 'notes': ''}]
        for resource_insert in resources_insert:

            description = resource_insert['description']
            notes = resource_insert['notes']
            application_id = application.id

            resources = db.session.query(models.Resources)
            resources = resources.filter(models.Resources.description == description)
            n_resources = resources.count()
            resources = resources.first()

            if n_resources == 0:
                resources = models.Resources(description=description, notes=notes,
                                             uuid=uuid.uuid4(), application_id=application_id)
                try:
                    db.session.add(resources)
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    print(e)


resources = db.session.query(models.Resources.id, models.Resources.description).\
    filter(models.Applications.description == 'INMOBILIARIA').all()

for resource in resources:
    resource_description = resource.description.lower()
    urls_insert = []
    urls_insert = [{'description': '/inmobiliaria/' + resource_description, 'notes': '', 'method': 'get'},
                   {'description': '/inmobiliaria/' + resource_description, 'notes': '', 'method': 'post'},
                   {'description': '/inmobiliaria/' + resource_description, 'notes': '', 'method': 'patch'},
                   {'description': '/inmobiliaria/' + resource_description, 'notes': '', 'method': 'put'}]
    for url_insert in urls_insert:
        description = url_insert['description']
        notes = url_insert['notes']
        method = url_insert['method']
        resource_id = resource.id

        urls = db.session.query(models.Urls)
        urls = urls.filter(models.Urls.description == description)
        urls = urls.filter(models.Urls.method == method)
        n_urls = urls.count()
        urls = urls.first()
        if n_urls == 0:
            urls = models.Urls(description=description, notes=notes, uuid=uuid.uuid4(),
                               resource_id=resource_id, method=method)
            try:
                db.session.add(urls)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                print(e)

groups = db.session.query(models.Groups.id, models.Groups.description).all()

for group in groups:
    urls = db.session.query(models.Urls.id, models.Urls.description).all()
    for url in urls:
        group_id = group.id
        url_id = url.id

        urls_groups = db.session.query(models.UrlsGroups)
        urls_groups = urls_groups.filter(models.UrlsGroups.group_id == group_id)
        urls_groups = urls_groups.filter(models.UrlsGroups.url_id == url_id)
        n_urls_groups = urls_groups.count()

        if n_urls_groups == 0:
            urls_groups = models.UrlsGroups(url_id=url_id, group_id=group_id)
            try:
                db.session.add(urls_groups)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                print(e)

users_insert = []

users_insert = [{'description': 'USER ROOT', 'notes': '', 'name': 'ADMIN SYS', 'last_name': 'ROOT',
                 'second_last_name': 'ROOT', 'user': 'root', 'password': 'moro58'}]

for user_insert in users_insert:

    groups = db.session.query(models.Groups.id, models.Groups.description)
    groups = groups.filter(models.Groups.description == 'ADMINS')
    groups = groups.first()

    group_id = groups.id
    description = user_insert['description']
    notes = user_insert['notes']
    name = user_insert['name']
    last_name = user_insert['last_name']
    second_last_name = user_insert['second_last_name']
    user = user_insert['user']
    password = user_insert['password']

    users = db.session.query(models.Users)
    users = users.filter(models.Users.user == user)
    n_users = users.count()
    users = users.first()

    if n_users == 0:
        users = models.Users(description=description, notes=notes, group_id=group_id,
                             name=name, last_name=last_name, second_last_name=second_last_name,
                             user=user, password=password, uuid=uuid.uuid4())
        try:
            db.session.add(users)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            print(e)