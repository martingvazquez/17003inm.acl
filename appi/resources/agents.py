from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, get_agents, access
import uuid


class Agents(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/agents', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='Name cannot be converted', required=True)
        parser.add_argument('last_name', type=str, help='Last name cannot be converted', required=True)
        parser.add_argument('second_last_name', type=str, help='second last name cannot be converted', required=True)
        parser.add_argument('phone_number', type=str, help='Phone number cannot be converted', required=True)
        parser.add_argument('email', type=str, help='Email cannot be converted', required=True)
        parser.add_argument('curp', type=str, help='curp cannot be converted', required=True)
        parser.add_argument('rfc', type=str, help='rfc cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted', required=True)
        parser.add_argument('agent_type_id', type=str, help='Agent_type_id cannot be converted', required=True)
        args = parser.parse_args()
        name = args['name']
        last_name = args['last_name']
        second_last_name = args['second_last_name']
        phone_number = args['phone_number']
        email = args['email']
        curp = args['curp']
        rfc = args['rfc']
        notes = args['notes']
        agent_type_id = args['agent_type_id']

        agents = models.Agents(name=name, last_name=last_name, second_last_name=second_last_name,
                               phone_number=phone_number, email=email, curp=curp, rfc=rfc,
                               notes=notes, uuid=uuid.uuid4(), insert_user_uuid=uuid.uuid4(),
                               update_user_uuid=uuid.uuid4(), company_uuid=uuid.uuid4(),
                               agent_type_id=agent_type_id)
        try:
            db.session.add(agents)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        agent_id = agents.id
        agents = get_agents(db, models)
        agents = agents.filter(models.Agents.id == agent_id)

        agents_schema = models.AgentSchema(many=True)
        agents = agents_schema.dump(agents).data

        return {'status': 'ok', 'agents': agents}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,agent_id=-1, all_show=0,
            column='', column2='', data_filter2=''):
        if access(db, models, '/acl/agents', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        agents = get_agents(db, models)
        agents_load = {}

        if agent_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                agents = agents.order_by(order)
                agents = agents.filter(
                    or_(
                        models.Agents.id == condition_int,
                        models.Agents.name.ilike(condition_string),
                        models.Agents.last_name.ilike(condition_string),
                        models.Agents.second_last_name.ilike(condition_string),
                        models.Agents.phone_number.ilike(condition_string),
                        models.Agents.email.ilike(condition_string),
                        models.Agents.curp.ilike(condition_string),
                        models.Agents.rfc.ilike(condition_string),
                        models.Agents.notes.ilike(condition_string),
                        models.Agent_types.description.ilike(condition_string)
                        # models.Agents.status == condition_int
                    )
                )

                n_rows = agents.count()

                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    agents = agents.limit(int(items_page)).offset(n_rows_show)
                agents_load['n_rows'] = n_rows
            else:
                if column == 'agents.status':
                    agents = agents.filter(models.Agents.status == data_filter)
                    n_rows = agents.count()
                    agents_load['n_rows'] = n_rows
        else:
            if agent_id >= 1:
                agents = agents.filter(models.Agents.id == agent_id)
                n_rows = agents.count()
                agents_load['n_rows'] = n_rows

        agents_schema = models.AgentSchema(many=True)
        agents = agents_schema.dump(agents).data
        print(agents)

        return {'agents':agents, 'n_rows':n_rows}

    def put(self, agent_id, token=''):
        if access(db, models, '/acl/agents', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        agents_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='Name cannot be converted', required=True)
        parser.add_argument('last_name', type=str, help='Last name cannot be converted', required=True)
        parser.add_argument('second_last_name', type=str, help='second last name cannot be converted', required=True)
        parser.add_argument('phone_number', type=str, help='Phone number cannot be converted', required=True)
        parser.add_argument('email', type=str, help='Email cannot be converted', required=True)
        parser.add_argument('curp', type=str, help='curp cannot be converted', required=True)
        parser.add_argument('rfc', type=str, help='rfc cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('agent_type_id', type=str, help='Agent_type_id cannot be converted')
        args = parser.parse_args()

        agents = db.session.query(models.Agents)
        agents = agents.filter(models.Agents.id == agent_id)

        n_rows = agents.count()

        try:
            agents.update(args)
            db.session.commit()
            agents = get_agents(db, models)
            agents = agents.filter(models.Agents.id == agent_id)
        except Exception as e:
            print(e)
            db.session.rollback()
            agents_schema = models.AgentSchema(many=True)
            agents = agents_schema.dump(agents)
            agents_load['agents'] = agents.data
            agents_load['n_rows'] = n_rows
            agents_load['status'] = 'false'
            return agents_load

        agents_schema = models.AgentSchema(many=True)
        agents = agents_schema.dump(agents).data

        return {'status': 'ok', 'agents': agents, 'n_rows': n_rows}

    def patch(self,agent_id, token=''):
        if access(db, models, '/acl/agents', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        agents = db.session.query(models.Agents)
        agents = agents.filter(models.Agents.id == agent_id)

        agents.update(args)
        db.session.commit()
        n_rows = agents.count()

        agents = get_agents(db, models)
        agents = agents.filter(models.Agents.id == agent_id)

        agents_schema = models.AgentSchema(many=True)
        agents = agents_schema.dump(agents).data

        return {'n_rows': n_rows, 'agents': agents}
