from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, get_cities, access, upper
import uuid
from datetime import datetime, timedelta


class Cities(Resource):
    def post(self, token=''):
        if access(db, models, '/acl/cities', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('state_id', type=int, help='Notes cannot be converted', required=True)
            args = parser.parse_args()

            description = args['description']
            notes = args['notes']
            state_id = args['state_id']

            if upper == 1:
                description = description.upper()
                if notes is not None:
                    notes = notes.upper()

            cities = models.Cities(description=description, notes=notes,state_id = state_id, uuid=uuid.uuid4())
            try:
                db.session.add(cities)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                return {'status': 'false'}

            city_id = cities.id
            cities = get_cities(db, models)
            cities = cities.filter(models.Cities.id == city_id)

            cities_schema = models.CitySchema(many=True)
            cities = cities_schema.dump(cities).data

            return {'status': 'ok', 'cities': cities}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def get(self, token, column_order_by='', type_order='', data_filter='', current_page=1, 
            items_page=5, city_id=-1, show_all=0, column='', column2='', data_filter2=''):
        if access(db, models, '/acl/cities', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}

        cities = get_cities(db, models)
        cities_load = {}
        if city_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%' + data_filter_string + '%'
                condition_int = data_filter_integer

                cities = cities.order_by(order)
                cities = cities.filter(
                    or_(
                        models.Cities.id == condition_int,
                        models.Cities.description.ilike(condition_string),
                        models.Cities.notes.ilike(condition_string),
                        models.States.description.ilike(condition_string),
                        models.Countries.description.ilike(condition_string)
                    )
                )
                n_rows = cities.count()
                if show_all == 0:
                    n_rows_show = (int(current_page) - 1) * int(items_page)
                    cities = cities.limit(int(items_page)).offset(n_rows_show)
                cities_load['n_rows'] = n_rows

            else:
                if column == 'cities.status':
                    cities = cities.filter(models.Cities.status == data_filter)

                if column == 'cities.state_id':
                    cities = cities.filter(models.Cities.state_id == data_filter)
                if column2 != '':
                    if column2 == 'cities.status':
                        cities = cities.filter(models.Cities.status == data_filter2 )

                n_rows = cities.count()
                cities_load['n_rows'] = n_rows
        else:
            if city_id >= 1:
                cities = cities.filter(models.Cities.id == city_id)
                n_rows = cities.count()
                cities_load['n_rows'] = n_rows
            else:
                if city_id == 0:
                    return{'error': '6'}

        cities_schema = models.CitySchema(many=True)
        cities = cities_schema.dump(cities).data

        return {'cities':cities, 'n_rows': n_rows}

    def put(self, city_id, token=''):
        if access(db, models, '/acl/cities', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        cities_load = {}
        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('state_id', type=int, help='State', required=True)
            args = parser.parse_args()
            cities = db.session.query(models.Cities)
            cities = cities.filter(models.Cities.id == city_id)
            n_rows = cities.count()
            try:
                cities.update(args)
                db.session.commit()
                cities = get_cities(db, models)
                cities = cities.filter(models.Cities.id == city_id)
            except Exception as e:
                db.session.rollback()
                cities_schema = models.CitySchema(many=True)
                cities = cities_schema.dump(cities)
                cities_load['cities'] = cities.data
                cities_load['n_rows'] = n_rows
                cities_load['status'] = 'false'
                return cities_load

            cities_schema = models.CitySchema(many=True)
            cities = cities_schema.dump(cities).data
            return  {'status': 'ok', 'cities': cities, 'n_rows': n_rows}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def patch(self, city_id, token=''):
        if access(db, models, '/acl/cities', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        cities = db.session.query(models.Cities)
        cities = cities.filter(models.Cities.id == city_id)

        cities.update(args)
        db.session.commit()
        n_rows = cities.count()

        cities = get_cities(db, models)
        cities = cities.filter(models.Cities.id == city_id)

        cities_schema = models.CitySchema(many=True)
        cities = cities_schema.dump(cities).data

        return {'n_rows': n_rows, 'cities': cities}
