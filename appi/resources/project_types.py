from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access
import uuid


class ProjectTypes(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/project_types', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        project_types_schema = models.ProjectTypeSchema()
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='Description cannot be converted')
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()
        
        description = args['description']
        notes = args['notes']

        project_types = models.Project_types(description=description, notes=notes, uuid=uuid.uuid4(),
                                             insert_user_uuid=uuid.uuid4(), update_user_uuid=uuid.uuid4(),
                                             company_uuid=uuid.uuid4())
        try:
            db.session.add(project_types)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        project_types = project_types_schema.dump(project_types)
        return {'status': 'ok', 'project_types': project_types}
 
    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5, project_type_id=-1,
            all_show=0, column='', column2='', data_filter2=''):
        if access(db, models, '/acl/project_types', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        project_types = db.session.query(models.Project_types)
        project_types_load = {}

        if project_type_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                project_types = project_types.order_by(order)
                project_types = project_types.filter(
                    or_(
                        models.Project_types.id == condition_int,
                        models.Project_types.description.ilike(condition_string),
                        models.Project_types.notes.ilike(condition_string)
                        # models.Project_types.status == condition_int
                    )
                )

                n_rows = project_types.count()

                if all_show == 0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    project_types = project_types.limit(int(items_page)).offset(n_rows_show)
                project_types_load['n_rows'] = n_rows
            else:
                if column == 'project_types.status':
                    project_types = project_types.filter(models.Project_types.status == data_filter)
                    n_rows = project_types.count()
                    project_types_load['n_rows'] = n_rows
        else:
            if project_type_id >= 1:
                project_types = project_types.filter(models.Project_types.id == project_type_id)
                n_rows = project_types.count()
                project_types_load['n_rows'] = n_rows

        project_types_schema = models.ProjectTypeSchema(many=True)
        project_types = project_types_schema.dump(project_types)
        project_types_load['project_types'] = project_types.data
        return project_types_load

    def put(self, project_type_id, token=''):
        if access(db, models, '/acl/project_types', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        project_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()

        project_types = db.session.query(models.Project_types)
        project_types = project_types.filter(models.Project_types.id == project_type_id)

        n_rows = project_types.count()

        try:
            project_types.update(args)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            project_types_schema = models.ProjectTypeSchema(many=True)
            project_types = project_types_schema.dump(project_types)
            project_types_load['project_types'] = project_types.data
            project_types_load['n_rows'] = n_rows
            project_types_load['status'] = 'false'
            return project_types_load

        project_types_schema = models.ProjectTypeSchema(many=True)
        project_types = project_types_schema.dump(project_types)
        project_types_load['project_types'] = project_types.data
        project_types_load['n_rows'] = n_rows
        project_types_load['status'] = 'ok'
        return project_types_load

    def patch(self, project_type_id, token=''):
        if access(db, models, '/acl/project_types', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        project_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        project_types = db.session.query(models.Project_types)
        project_types = project_types.filter(models.Project_types.id == project_type_id)

        project_types.update(args)
        db.session.commit()
        n_rows = project_types.count()

        project_types_schema = models.ClientTypeSchema(many=True)
        project_types = project_types_schema.dump(project_types)
        
        project_types_load['project_types'] = project_types.data
        project_types_load['n_rows'] = n_rows
        
        return project_types_load
