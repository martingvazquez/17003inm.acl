from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access
import uuid


class FinancingTypes(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/financing_types', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description',  help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()

        description = args['description']
        notes = args['notes']

        financing_types = models.Financing_types(description=description, notes=notes, uuid=uuid.uuid4(),
                                           insert_user_uuid=uuid.uuid4(), update_user_uuid=uuid.uuid4(),
                                           company_uuid=uuid.uuid4())
        try:
            db.session.add(financing_types)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        financing_types = db.session.query(models.Financing_types)
        financing_types_schema = models.FinancingTypeSchema(many=True)
        financing_types = financing_types_schema.dump(financing_types)
        return {'status': 'ok','financing_types': financing_types}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,financing_type_id=-1,
            all_show=0, column=''):
        if access(db, models, '/acl/financing_types', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        financing_types = db.session.query(models.Financing_types)
        financing_types_load = {}

        if financing_type_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                financing_types = financing_types.order_by(order)
                financing_types = financing_types.filter(
                    or_(
                        models.Financing_types.id == condition_int,
                        models.Financing_types.description.ilike(condition_string),
                        models.Financing_types.notes.ilike(condition_string)
                    )
                )
                n_rows = financing_types.count()
                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    financing_types = financing_types.limit(int(items_page)).offset(n_rows_show)
                financing_types_load['n_rows'] = n_rows
            else:
                if column == 'financing_types.status':
                    financing_types = financing_types.filter(models.Financing_types.status == data_filter)
                    n_rows = financing_types.count()
                    financing_types_load['n_rows'] = n_rows
        else:
            if financing_type_id >= 1:
                financing_types = financing_types.filter(models.Financing_types.id == financing_type_id)
                n_rows = financing_types.count()
                financing_types_load['n_rows'] = n_rows

        financing_types_schema = models.FinancingTypeSchema(many=True)
        financing_types = financing_types_schema.dump(financing_types)
        financing_types_load['financing_types'] = financing_types.data

        print(financing_types_load)

        return financing_types_load

    def put(self, financing_type_id, token=''):
        if access(db, models, '/acl/financing_types', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        financing_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')

        args = parser.parse_args()

        financing_types = db.session.query(models.Financing_types)
        financing_types = financing_types.filter(models.Financing_types.id == financing_type_id)
        n_rows = financing_types.count()

        try:
            financing_types.update(args)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            financing_types_schema = models.FinancingTypeSchema(many=True)
            financing_types = financing_types_schema.dump(financing_types)
            financing_types_load['financing_types'] = financing_types.data
            financing_types_load['n_rows'] = n_rows
            financing_types_load['status'] = 'false'
            return financing_types_load

        financing_types_schema = models.FinancingTypeSchema(many=True)
        financing_types = financing_types_schema.dump(financing_types)
        financing_types_load['financing_types'] = financing_types.data
        financing_types_load['n_rows'] = n_rows
        financing_types_load['status'] = 'ok'
        return financing_types_load

    def patch(self, financing_type_id, token=''):
        if access(db, models, '/acl/financing_types', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        financing_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        financing_types = db.session.query(models.Financing_types)
        financing_types = financing_types.filter(models.Financing_types.id == financing_type_id)

        financing_types.update(args)
        db.session.commit()
        n_rows = financing_types.count()
        financing_types_schema = models.FinancingTypeSchema(many=True)
        financing_types = financing_types_schema.dump(financing_types)
        financing_types_load['financing_types'] = financing_types.data
        financing_types_load['n_rows'] = n_rows
        return financing_types_load
