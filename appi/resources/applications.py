from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access, upper, language
import uuid


class Applications(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/applications', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        applications_schema = models.ApplicationSchema()
        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description',  help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('port', type=str, help='Notes cannot be converted')
            args = parser.parse_args()

            description = args['description']
            notes = args['notes']
            port = args['port']

            if upper == 1:
                description = description.upper()
                if notes is not None:
                    notes = notes.upper()

            applications = models.Applications(description=description, notes=notes, uuid=uuid.uuid4(),
                                               port=port)
            try:
                db.session.add(applications)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                if language == 1:
                    return {'error': '5', 'message': 'Error descripción repetida'}
                else:
                    return {'error': '5', 'message': 'Error description repeat'}

            applications = applications_schema.dump(applications)
            return {'status': 'ok', 'applications': applications}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, 
            items_page=5, application_id=-1, show_all=0, column=''):

        if access(db, models, '/acl/applications', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        applications = db.session.query(models.Applications)
        applications_load = {}

        if application_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                applications = applications.order_by(order)
                applications = applications.filter(
                    or_(
                        models.Applications.id == condition_int,
                        models.Applications.description.ilike(condition_string),
                        models.Applications.notes.ilike(condition_string)
                    )
                )
                n_rows = applications.count()
                if show_all == 0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    applications = applications.limit(int(items_page)).offset(n_rows_show)
                applications_load['n_rows'] = n_rows
            else:
                if column == 'applications.status':
                    applications = applications.filter(models.Applications.status == data_filter)
                    n_rows = applications.count()
                    applications_load['n_rows'] = n_rows

        else:
            if application_id >= 1:
                applications = applications.filter(models.Applications.id == application_id)
                n_rows = applications.count()
                applications_load['n_rows'] = n_rows

        applications_schema = models.ApplicationSchema(many=True)
        applications = applications_schema.dump(applications)
        applications_load['applications'] = applications.data
        return applications_load

    def put(self, application_id, token=''):
        if access(db, models, '/acl/applications', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        applications_load = {}
        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('port', type=int, help='Notes cannot be converted', required=True)
            args = parser.parse_args()
            applications = db.session.query(models.Applications)
            applications = applications.filter(models.Applications.id == application_id)

            n_rows = applications.count()

            try:
                applications.update(args)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                applications_schema = models.ApplicationSchema(many=True)
                applications = applications_schema.dump(applications)
                applications_load['applications'] = applications.data
                applications_load['n_rows'] = n_rows
                applications_load['status'] = 'false'
                return applications_load

            applications_schema = models.ApplicationSchema(many=True)
            applications = applications_schema.dump(applications)
            applications_load['applications'] = applications.data
            applications_load['n_rows'] = n_rows
            applications_load['status'] = 'ok'

            return applications_load
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406


    def patch(self, application_id, token=''):
        if access(db, models, '/acl/applications', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        applications_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        applications = db.session.query(models.Applications)
        applications = applications.filter(models.Applications.id == application_id)

        applications.update(args)
        db.session.commit()

        n_rows = applications.count()
        applications_schema = models.ApplicationSchema(many=True)
        applications = applications_schema.dump(applications)
        applications_load['applications'] = applications.data
        applications_load['n_rows'] = n_rows
        return applications_load
