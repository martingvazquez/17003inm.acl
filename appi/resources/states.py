from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, get_states, access, upper
import uuid
from datetime import datetime, timedelta

class States(Resource):
    
    def post(self, token):
        if access(db, models, '/acl/states', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('country_id', type=int, help='Notes cannot be converted', required=True)
            args = parser.parse_args()

            description = args['description']
            notes = args['notes']
            country_id = args['country_id']

            if upper == 1:
                description = description.upper()
                if notes is not None:
                    notes = notes.upper()

            states = models.States(description=description, notes=notes, country_id=country_id,
                                   uuid=uuid.uuid4())
            try:
                db.session.add(states)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                return {'error': '5', 'message': 'Error description repeat'}

            state_id = states.id
            states = get_states(db, models)
            states = states.filter(models.States.id == state_id)

            states_schema = models.StateSchema(many=True)
            states = states_schema.dump(states).data

            return {'status': 'ok', 'states': states}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def get(self, token, column_order_by='', type_order='', data_filter='', current_page=1, 
            items_page=5, state_id=-1, show_all=0, column='', column2='', data_filter2=''):
        if access(db, models, '/acl/states', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        states = get_states(db, models)
        states_load = {}

        if state_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)

                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%' + data_filter_string + '%'
                condition_int = data_filter_integer

                states = states.order_by(order)
                states = states.filter(
                    or_(
                        models.States.id == condition_int,
                        models.States.description.ilike(condition_string),
                        models.States.notes.ilike(condition_string),
                        models.Countries.description.ilike(condition_string)
                    )
                )
                n_rows = states.count()
                if show_all == 0:
                    n_rows_show = (int(current_page) - 1) * int(items_page)
                    states = states.limit(int(items_page)).offset(n_rows_show)
                states_load['n_rows'] = n_rows
            else:
                if column == 'states.status':
                    states = states.filter(models.States.status == data_filter)

                if column == 'states.country_id':
                    states = states.filter(models.States.country_id == data_filter)
                if column2 != '':
                    if column2 == 'states.status':
                        states = states.filter(models.States.status == data_filter2 )

                n_rows = states.count()
                states_load['n_rows'] = n_rows
        else:
            if state_id >= 1:
                states = states.filter(models.States.id == state_id)
                n_rows = states.count()
                states_load['n_rows'] = n_rows

        states_schema = models.StateSchema(many=True)
        states = states_schema.dump(states).data

        return {'states': states, 'n_rows': n_rows}

    def put(self, state_id, token=''):
        if access(db, models, '/acl/states', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        states_load = {}
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('country_id', type=int, help='Country', required=True)
            args = parser.parse_args()
            states = db.session.query(models.States)
            states = states.filter(models.States.id == state_id)
            n_rows = states.count()
            try:
                states.update(args)
                db.session.commit()
                states = get_states(db, models)
                states = states.filter(models.States.id == state_id)
            except Exception as e:
                print(e)
                db.session.rollback()
                states_schema = models.StateSchema(many=True)
                states = states_schema.dump(states)
                states_load['states'] = states.data
                states_load['n_rows'] = n_rows
                states_load['status'] = 'false'
                return states_load

            states_schema = models.StateSchema(many=True)
            states = states_schema.dump(states).data
            return {'status': 'ok', 'states': states, 'n_rows': n_rows}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def patch(self, state_id, token=''):
        if access(db, models, '/acl/states', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        states = db.session.query(models.States)
        states = states.filter(models.States.id == state_id)

        states.update(args)
        db.session.commit()
        n_rows = states.count()

        states = get_states(db, models)
        states = states.filter(models.States.id == state_id)

        states_schema = models.StateSchema(many=True)
        states = states_schema.dump(states).data

        return {'n_rows': n_rows, 'states': states}