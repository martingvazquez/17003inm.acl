from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access
import uuid


class AgentTypes(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/agent_types', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description',  help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()

        description = args['description']
        notes = args['notes']

        agent_types = models.Agent_types(description=description, notes=notes, uuid=uuid.uuid4(),
                                         insert_user_uuid=uuid.uuid4(), update_user_uuid=uuid.uuid4(),
                                         company_uuid=uuid.uuid4())
        try:
            db.session.add(agent_types)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        agent_types = db.session.query(models.Agent_types)
        agent_types_schema = models.AgentTypeSchema(many=True)
        agent_types = agent_types_schema.dump(agent_types)
        return {'status': 'ok','agent_types': agent_types}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5, agent_type_id=-1,
            all_show=0, column=''):
        if access(db, models, '/acl/agent_types', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        agent_types = db.session.query(models.Agent_types)
        agent_types_load = {}

        if agent_type_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                agent_types = agent_types.order_by(order)
                agent_types = agent_types.filter(
                    or_(
                        models.Agent_types.id == condition_int,
                        models.Agent_types.description.ilike(condition_string),
                        models.Agent_types.notes.ilike(condition_string)
                    )
                )
                n_rows = agent_types.count()
                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    agent_types = agent_types.limit(int(items_page)).offset(n_rows_show)
                agent_types_load['n_rows'] = n_rows
            else:
                if column == 'agent_types.status':
                    agent_types = agent_types.filter(models.Agent_types.status == data_filter)
                    n_rows = agent_types.count()
                    agent_types_load['n_rows'] = n_rows
        else:
            if agent_type_id >= 1:
                agent_types = agent_types.filter(models.Agent_types.id == agent_type_id)
                n_rows = agent_types.count()
                agent_types_load['n_rows'] = n_rows

        agent_types_schema = models.AgentTypeSchema(many=True)
        agent_types = agent_types_schema.dump(agent_types)
        agent_types_load['agent_types'] = agent_types.data

        print(agent_types_load)

        return agent_types_load

    def put(self,agent_type_id, token=''):
        if access(db, models, '/acl/agent_types', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        agent_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')

        args = parser.parse_args()

        agent_types = db.session.query(models.Agent_types)
        agent_types = agent_types.filter(models.Agent_types.id == agent_type_id)
        n_rows = agent_types.count()

        try:
            agent_types.update(args)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            agent_types_schema = models.AgentTypeSchema(many=True)
            agent_types = agent_types_schema.dump(agent_types)
            agent_types_load['agent_types'] = agent_types.data
            agent_types_load['n_rows'] = n_rows
            agent_types_load['status'] = 'false'
            return agent_types_load

        agent_types_schema = models.AgentTypeSchema(many=True)
        agent_types = agent_types_schema.dump(agent_types)
        agent_types_load['agent_types'] = agent_types.data
        agent_types_load['n_rows'] = n_rows
        agent_types_load['status'] = 'ok'
        return agent_types_load

    def patch(self, agent_type_id, token=''):
        if access(db, models, '/acl/agent_types', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        agent_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        agent_types = db.session.query(models.Agent_types)
        agent_types = agent_types.filter(models.Agent_types.id == agent_type_id)

        agent_types.update(args)
        db.session.commit()
        n_rows = agent_types.count()

        agent_types_schema = models.AgentTypeSchema(many=True)
        agent_types = agent_types_schema.dump(agent_types)

        agent_types_load['agent_types'] = agent_types.data
        agent_types_load['n_rows'] = n_rows

        return agent_types_load
