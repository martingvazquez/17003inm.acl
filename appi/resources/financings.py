from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access, get_financings
import uuid


class Financings(Resource):

    def post(self, token='',):
        if access(db, models, '/acl/financings', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('financing_type_id', type=str, help='Financing_type_id cannot be converted')
        args = parser.parse_args()


        description = args['description']
        notes = args['notes']
        financing_type_id = args['financing_type_id']

        financings = models.Financings(description=description, notes=notes, uuid=uuid.uuid4(),
                                       insert_user_uuid=uuid.uuid4(),update_user_uuid=uuid.uuid4(),
                                       company_uuid=uuid.uuid4(), financing_type_id = financing_type_id )

        try:
            db.session.add(financings)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        financing_id = financings.id
        financings = get_financings(db, models)
        financings = financings.filter(models.Financings.id == financing_id)

        financings_schema = models.FinancingSchema(many=True)
        financings = financings_schema.dump(financings).data

        return {'status': 'ok', 'financings': financings}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,financing_id=-1,
            all_show=0,column='', column2='', data_filter2=''):
        if access(db, models, '/acl/financings', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        financings = get_financings(db, models)
        financings_load = {}

        if financing_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                financings = financings.filter(
                    or_(
                        models.Financings.id == condition_int,
                        models.Financings.description.ilike(condition_string),
                        models.Financings.notes.ilike(condition_string),
                        models.Financing_types.description.ilike(condition_string)
                        # models.Financings.status == condition_int
                    )
                )
                financings = financings.order_by(order)
                n_rows = financings.count()

                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    financings = financings.limit(int(items_page)).offset(n_rows_show)
                financings_load['n_rows'] = n_rows
            else :
                if column == 'financings.financing_type_id':
                    financings = financings.filter(models.Financings.financing_type_id == data_filter)
                if column == 'financings.status':
                    financings = financings.filter(models.Financings.status == data_filter)
                if column2 != '':
                    if column2 == 'clients.status':
                        financings = financings.filter(models.Financings.status == data_filter2)

                n_rows = financings.count()
                financings_load['n_rows'] = n_rows
        else:
            if financing_id >= 1:
                financings = financings.filter(models.Financings.id == financing_id)
                n_rows = financings.count()
                financings_load['n_rows'] = n_rows

        financings_schema = models.FinancingSchema(many=True)
        financings = financings_schema.dump(financings).data
        print(financings)

        return {'financings': financings, 'n_rows': n_rows}

    def put(self, financing_id, token='',):
        if access(db, models, '/acl/financings', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        financings_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('financing_type_id', type=str, help='Financing_type_id cannot be converted')
        args = parser.parse_args()

        financings = db.session.query(models.Financings)
        financings = financings.filter(models.Financings.id == financing_id)
        n_rows = financings.count()

        try:
            financings.update(args)
            db.session.commit()
            financings = get_financings(db, models)
            financings = financings.filter(models.Financings.id == financing_id)
        except Exception as e:
            print(e)
            db.session.rollback()
            financings_schema = models.FinancingSchema(many=True)
            financings = financings_schema.dump(financings)
            financings_load['financings'] = financings.data
            financings_load['n_rows'] = n_rows
            financings_load['status'] = 'false'
            return financings_load

        financings_schema = models.FinancingSchema(many=True)
        financings = financings_schema.dump(financings).data

        return {'status': 'ok', 'financings': financings, 'n_rows': n_rows}

    def patch(self,financing_id, token=''):
        if access(db, models, '/acl/financings', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        financings = db.session.query(models.Financings)
        financings = financings.filter(models.Financings.id == financing_id)

        financings.update(args)
        db.session.commit()
        n_rows = financings.count()

        financings = get_financings(db, models)
        financings = financings.filter(models.Financings.id == financing_id)

        financings_schema = models.FinancingSchema(many=True)
        financings = financings_schema.dump(financings).data

        return {'n_rows': n_rows, 'financings': financings}
