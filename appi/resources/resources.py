from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, get_resources, access, upper, language
import uuid


class Resources(Resource):
    def post(self, token=''):
        if access(db, models, '/acl/resources', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('application_id', type=int, help='Notes cannot be converted', required=True)
            args = parser.parse_args()

            description = args['description']
            notes = args['notes']
            application_id = args['application_id']

            if upper == 1:
                description = description.upper()
                if notes is not None:
                    notes = notes.upper()

            resources = models.Resources(description=description, notes=notes,application_id=application_id, uuid=uuid.uuid4())
            try:
                db.session.add(resources)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                if language == 1:
                    return {'error': '5', 'message': 'Error descripción repetida'}
                else:
                    return {'error': '5', 'message': 'Error description repeat'}

            resource_id = resources.id
            resources = get_resources(db, models)
            resources = resources.filter(models.Resources.id == resource_id)

            resources_schema = models.ResourceSchema(many=True)
            resources = resources_schema.dump(resources).data

            return {'status': 'ok', 'resources': resources}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406


    def get(self, token='', column_order_by='', type_order='', data_filter='', 
            current_page=1, items_page=5, resource_id=-1, show_all=0, column='',
            column2='', data_filter2=''):

        if access(db, models, '/acl/resources', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        resources = get_resources(db, models)
        resources_load = {}

        if resource_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)

                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%' + data_filter_string + '%'
                condition_int = data_filter_integer

                resources = resources.order_by(order)
                resources = resources.filter(
                    or_(
                        models.Resources.id == condition_int,
                        models.Resources.description.ilike(condition_string),
                        models.Resources.notes.ilike(condition_string),
                        models.Applications.description.ilike(condition_string)
                    )
                )
                n_rows = resources.count()
                if show_all == 0:
                    n_rows_show = (int(current_page) - 1) * int(items_page)
                    resources = resources.limit(int(items_page)).offset(n_rows_show)
                resources_load['n_rows'] = n_rows
            else:
                if column == 'resources.status':
                    resources = resources.filter(models.Resources.status == data_filter)

                if column == 'resources.application_id':
                    resources = resources.filter(models.Resources.application_id == data_filter)
                if column2 != '':
                    if column2 == 'resources.status':
                        resources = resources.filter(models.Resources.status == data_filter2 )

                n_rows = resources.count()
                resources_load['n_rows'] = n_rows
        else:
            if resource_id >= 1:
                resources = resources.filter(models.Resources.id == resource_id)
                n_rows = resources.count()
                resources_load['n_rows'] = n_rows

        resources_schema = models.ResourceSchema(many=True)
        resources = resources_schema.dump(resources).data

        return {'resources': resources, 'n_rows': n_rows}

    def put(self,  resource_id, token=''):
        if access(db, models, '/acl/resources', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        resources_load = {}
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('application_id', type=int, help='Application', required=True)
            args = parser.parse_args()
            resources = db.session.query(models.Resources)
            resources = resources.filter(models.Resources.id == resource_id)
            n_rows = resources.count()
            try:
                resources.update(args)
                db.session.commit()
                resources = get_resources(db, models)
                resources = resources.filter(models.Resources.id == resource_id)
            except Exception as e:
                print(e)
                db.session.rollback()
                resources_schema = models.ResourceSchema(many=True)
                resources = resources_schema.dump(resources)
                resources_load['resources'] = resources.data
                resources_load['n_rows'] = n_rows
                resources_load['status'] = 'false'
                return resources_load
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

        resources_schema = models.ResourceSchema(many=True)
        resources = resources_schema.dump(resources).data
        return{'status': 'ok', 'resources': resources, 'n_rows': n_rows}

    def patch(self, resource_id, token=''):
        if access(db, models, '/acl/resources', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        resources = db.session.query(models.Resources)
        resources = resources.filter(models.Resources.id == resource_id)

        resources.update(args)
        db.session.commit()
        n_rows = resources.count()

        resources = get_resources(db, models)
        resources = resources.filter(models.Resources.id ==resource_id)

        resources_schema = models.ResourceSchema(many=True)
        resources = resources_schema.dump(resources).data

        return {'n_rows': n_rows, 'resources': resources}
