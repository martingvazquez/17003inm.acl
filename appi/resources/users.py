from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, get_users, access, language
import uuid


class Users(Resource):
    def post(self, token=''):
        if access(db, models, '/acl/users', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('group_id', type=int, help='Notes cannot be converted', required=True)
            parser.add_argument('name', type=str, help='Notes cannot be converted', required=True)
            parser.add_argument('last_name', type=str, help='Notes cannot be converted', required=True)
            parser.add_argument('second_last_name', type=str, help='Notes cannot be converted')
            parser.add_argument('user', type=str, help='Notes cannot be converted', required=True)
            parser.add_argument('password', type=str, help='Notes cannot be converted', required=True)
            args = parser.parse_args()

            description = args['description']
            notes = args['notes']
            group_id = args['group_id']
            name = args['name']
            last_name = args['last_name']
            second_last_name = args['second_last_name']
            user = args['user']
            password = args['password']

            users = models.Users(description=description,
                                 notes=notes,
                                 group_id=group_id,
                                 name=name,
                                 last_name=last_name,
                                 second_last_name=second_last_name,
                                 user=user,
                                 password=password,
                                 uuid=uuid.uuid4())
            try:
                db.session.add(users)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                if language == 1:
                    return {'error': '5', 'message': 'Error nombre de usuario y contraseña ya existen'}
                else:
                    return {'error': '5', 'message': 'Error user and password already exist'}

            user_id = users.id
            users = get_users(db, models)
            users = users.filter(models.Users.id == user_id)

            users_schema = models.UserSchema(many=True)
            users = users_schema.dump(users).data

            return {'status': 'ok', 'users': users}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406


    def get(self, token, column_order_by='', type_order='', data_filter='', current_page=1,
            items_page=5, user_id=-1, show_all=0, column='', column2='', data_filter2=''):

        if access(db, models, '/acl/users', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401


        users = get_users(db, models)
        users_load = {}

        if user_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)

                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%' + data_filter_string + '%'
                condition_int = data_filter_integer

                users = users.order_by(order)
                users = users.filter(
                    or_(
                        models.Users.id == condition_int,
                        models.Users.description.ilike(condition_string),
                        models.Users.notes.ilike(condition_string),
                        models.Groups.description.ilike(condition_string)
                    )
                )
                n_rows = users.count()
                if show_all == 0:
                    n_rows_show = (int(current_page) - 1) * int(items_page)
                    users = users.limit(int(items_page)).offset(n_rows_show)
                users_load['n_rows'] = n_rows
            else:
                if column == 'users.status':
                    users = users.filter(models.Users.status == data_filter)

                if column == 'users.group_id':
                    users = users.filter(models.Users.group_id == data_filter)
                if column2 != '':
                    if column2 == 'users.status':
                        users = users.filter(models.Users.status == data_filter2 )

                n_rows = users.count()
                users_load['n_rows'] = n_rows
        else:
            if user_id >= 1:
                users = users.filter(models.Users.id == user_id)
                n_rows = users.count()
                users_load['n_rows'] = n_rows

        users_schema = models.UserSchema(many=True)
        users = users_schema.dump(users).data

        return {'users': users, 'n_rows': n_rows}

    def put(self, user_id, token=''):
        if access(db, models, '/acl/users', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        users_load = {}
        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted')
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('group_id', type=int, help='Group Required', required=True)
            parser.add_argument('user', type=str, help='User', required=True)
            parser.add_argument('last_name', type=str, help='Last Name', required=True)
            parser.add_argument('second_last_name', type=str, help='Second Last Name')
            parser.add_argument('password', type=str, help='Password')
            parser.add_argument('name', type=str, help='name', required=True)
            args = parser.parse_args()
            users = db.session.query(models.Users)
            users = users.filter(models.Users.id == user_id)
            n_rows = users.count()
            try:
                users.update(args)
                db.session.commit()
                users = get_users(db, models)
                users = users.filter(models.Users.id == user_id)
            except Exception as e:
                print(e)
                db.session.rollback()
                users_schema = models.UserSchema(many=True)
                users = users_schema.dump(users)
                users_load['users'] = users.data
                users_load['n_rows'] = n_rows
                users_load['status'] = 'false'
                return users_load

            users_schema = models.UserSchema(many=True)
            users = users_schema.dump(users).data

            return {'status': 'ok', 'users': users, 'n_rows': n_rows}, 201, {'Etag': 'some-opaque-string'}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def patch(self, user_id, token=''):
        if access(db, models, '/acl/users', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        users = db.session.query(models.Users)
        users = users.filter(models.Users.id == user_id)

        users.update(args)
        db.session.commit()
        n_rows = users.count()

        users = get_users(db, models)
        users = users.filter(models.Users.id == user_id)

        users_schema = models.UserSchema(many=True)
        users = users_schema.dump(users).data

        return {'n_rows': n_rows, 'users': users}
