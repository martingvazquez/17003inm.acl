from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access, get_clients, get_projects, get_locations
import uuid


class Clients(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/clients', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='Name cannot be converted', required=True)
        parser.add_argument('last_name', type=str, help='Last name cannot be converted', required=True)
        parser.add_argument('second_last_name', type=str, help='second last name cannot be converted')
        parser.add_argument('phone_number', type=str, help='Phone number cannot be converted', required=True)
        parser.add_argument('email', type=str, help='Email cannot be converted')
        parser.add_argument('curp', type=str, help='curp cannot be converted', required=True)
        parser.add_argument('rfc', type=str, help='rfc cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('gender', type=str, help='Gender cannot be converted')
        parser.add_argument('birth_date', type=str, help='Birth_date cannot be converted')
        parser.add_argument('place_of_birth', type=str, help='Place_of_birth cannot be converted')
        parser.add_argument('purchase_range_initial', type=str, help='Purchase_range_initial cannot be converted')
        parser.add_argument('purchase_range_final', type=str, help='Purchase_range_final cannot be converted')
        parser.add_argument('client_type_id', type=str, help='Client_type_id cannot be converted', required=True)
        parser.add_argument('financing_id', type=str, help='financing_id cannot be converted', required=True)
        parser.add_argument('agent_id', type=str, help='Agent_id cannot be converted', required=True)
        args = parser.parse_args()

        name = args['name']
        last_name = args['last_name']
        second_last_name = args['second_last_name']
        phone_number = args['phone_number']
        email = args['email']
        curp = args['curp']
        rfc = args['rfc']
        notes = args['notes']
        gender = args['gender']
        birth_date = args['birth_date']
        place_of_birth = args['place_of_birth']
        purchase_range_initial = args['purchase_range_initial']
        purchase_range_final = args['purchase_range_final']
        client_type_id = args['client_type_id']
        financing_id = args['financing_id']
        agent_id = args['agent_id']

        clients = models.Clients(name=name, last_name=last_name, second_last_name=second_last_name,
                                 phone_number=phone_number, email=email, curp=curp, rfc=rfc, gender=gender,
                                 birth_date=birth_date, place_of_birth=place_of_birth,
                                 purchase_range_initial=purchase_range_initial,
                                 purchase_range_final=purchase_range_final, notes=notes, uuid=uuid.uuid4(),
                                 city_uuid=uuid.uuid4(),insert_user_uuid=uuid.uuid4(),
                                 update_user_uuid=uuid.uuid4(), company_uuid=uuid.uuid4(),
                                 client_type_id=client_type_id, financing_id=financing_id, agent_id=agent_id)
        try:
            db.session.add(clients)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        client_id = clients.id
        clients = get_clients(db, models)
        clients = clients.filter(models.Clients.id == client_id)

        clients_schema = models.ClientSchema(many=True)
        clients = clients_schema.dump(clients).data

        return {'status': 'ok', 'clients': clients}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,
            client_id=-1, all_show=0, column='', location=0):
        if access(db, models, '/acl/clients', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        clients = get_clients(db, models)
        clients_load = {}

        if location == 0:
            if client_id == -1:
                if column == '':
                    order = get_order_by(column_order_by, type_order)
                    data_filter_integer, data_filter_string = get_data_filter(data_filter)

                    condition_string = '%'+data_filter_string+'%'
                    condition_int = data_filter_integer

                    clients = clients.order_by(order)
                    clients = clients.filter(
                        or_(
                            models.Clients.id == condition_int,
                            models.Clients.name.ilike(condition_string),
                            models.Clients.last_name.ilike(condition_string),
                            models.Clients.second_last_name.ilike(condition_string),
                            models.Clients.phone_number.ilike(condition_string),
                            models.Clients.email.ilike(condition_string),
                            models.Clients.curp.ilike(condition_string),
                            models.Clients.rfc.ilike(condition_string),
                            models.Clients.notes.ilike(condition_string),
                            models.Clients.gender.ilike(condition_string),
                            models.Clients.birth_date.ilike(condition_string),
                            models.Clients.place_of_birth.ilike(condition_string),
                            models.Clients.purchase_range_initial == condition_int,
                            models.Clients.purchase_range_final == condition_int,
                            models.ClientTypes.description.ilike(condition_string),
                            models.Financings.description.ilike(condition_string),
                            models.Agents.name.ilike(condition_string)
                            # models.Clients.status == condition_int
                        )
                    )

                    n_rows = clients.count()

                    if all_show == 0:
                        n_rows_show = (int(current_page)-1) * int(items_page)
                        clients = clients.limit(int(items_page)).offset(n_rows_show)
                    clients_load['n_rows'] = n_rows
                else:
                    if column == 'clients.status':
                        clients = clients.filter(models.Clients.status == data_filter)
                        n_rows = clients.count()
                        clients_load['n_rows'] = n_rows
            else:
                if client_id >= 1:
                    clients = clients.filter(models.Clients.id == client_id)
                    n_rows = clients.count()
                    clients_load['n_rows'] = n_rows

            clients_schema = models.ClientSchema(many=True)
            clients = clients_schema.dump(clients).data
            print(clients)

            return {'clients': clients, 'n_rows': n_rows}
        else:
            locations = {}
            clients = clients.filter(models.Clients.id == client_id)
            clients_schema = models.ClientSchema(many=True)
            clients = clients_schema.dump(clients)
            clients = clients.data

            for clientJson in clients:
                client_id = clientJson['id']
                client_name = clientJson['name']
                client_last_name = clientJson['last_name']
                client_second_last_name = clientJson['second_last_name']
                client_type_id = clientJson['client_type_id']
                client_type = clientJson['client_type']

                locations['client_id'] = client_id
                locations['client_name'] = client_name
                locations['client_last_name']= client_last_name
                locations['client_second_last_name'] = client_second_last_name
                locations['client_type_id'] = client_type_id
                locations['client_type'] = client_type

                project_types = db.session.query(models.Project_types)
                project_types_load = {}
                project_types_schema = models.ProjectTypeSchema(many=True)
                project_types = project_types_schema.dump(project_types)
                project_types = project_types.data
                print('project_types')
                print(project_types)
                project_types_array = []
                for project_typeJson in project_types:
                    projects_types_json = {}
                    project_type_id = project_typeJson['id']
                    project_type = project_typeJson['description']
                    projects_types_json['project_type_id'] = project_type_id
                    projects_types_json['project_type'] = project_type

                    projects = get_projects(db, models)

                    projects = projects.filter(models.Projects.project_type_id == project_type_id)
                    projects_schema = models.projectSchema(many=True)
                    projects = projects_schema.dump(projects)
                    projects = projects.data
                    print('projects')
                    print(projects)
                    projects_array = []
                    for project_json in projects:
                        projects_json = {}
                        project_id = project_json['id']
                        project = project_json['description']

                        projects_json['project_id'] = project_id
                        projects_json['project'] = project

                        locations = get_locations(db,models)
                        locations = locations.filter(models.Locations.project_id == project_id)

                        location_schema = models.LocationSchema(many=True)

                        locations = location_schema.dump(locations)

                        locations = locations.data

                        locations_array = []

                        for locationJson in locations:
                            locations_json = {}

                            location_id = locationJson['id']
                            location = locationJson['description']
                            location_status = locationJson['status']

                            locations_json['location_id'] = location_id
                            locations_json['location'] = location
                            locations_json['location_status'] = location_status

                            locations_array.append(locations_json)
                            projects_json['locations'] = locations_array

                        projects_array.append(projects_json)
                        projects_types_json['projects'] = projects_array
                    project_types_array.append(projects_types_json)

                print('project_types_array')
                print(project_types_array)

                locations['project_types'] = project_types_array
            return locations

    def put(self, client_id, token=''):
        if access(db, models, '/acl/clients', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        clients_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='Name cannot be converted', required=True)
        parser.add_argument('last_name', type=str, help='Last name cannot be converted', required=True)
        parser.add_argument('second_last_name', type=str, help='second last name cannot be converted')
        parser.add_argument('phone_number', type=str, help='Phone number cannot be converted')
        parser.add_argument('email', type=str, help='Email cannot be converted', required=True)
        parser.add_argument('curp', type=str, help='curp cannot be converted', required=True)
        parser.add_argument('rfc', type=str, help='rfc cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('gender', type=str, help='Gender cannot be converted')
        parser.add_argument('birth_date', type=str, help='Birth_date cannot be converted')
        parser.add_argument('place_of_birth', type=str, help='Place_of_birth cannot be converted')
        parser.add_argument('purchase_range_initial', type=str, help='Purchase_range_initial cannot be converted')
        parser.add_argument('purchase_range_final', type=str, help='Purchase_range_final cannot be converted')
        parser.add_argument('client_type_id', type=str, help='Client_type_id cannot be converted', required=True)
        parser.add_argument('financing_id', type=str, help='Financing cannot be converted', required=True)
        parser.add_argument('agent_id', type=str, help='Agent_id cannot be converted', required=True)

        args = parser.parse_args()

        clients = db.session.query(models.Clients)
        clients = clients.filter(models.Clients.id == client_id)

        n_rows = clients.count()

        try:
            clients.update(args)
            db.session.commit()
            clients = get_clients(db, models)
            clients = clients.filter(models.Clients.id == client_id)
        except Exception as e:
            print(e)
            db.session.rollback()
            clients_schema = models.ClientSchema(many=True)
            clients = clients_schema.dump(clients)
            clients_load['clients'] = clients.data
            clients_load['n_rows'] = n_rows
            clients_load['status'] = 'false'
            return clients_load

        clients_schema = models.ClientSchema(many=True)
        clients = clients_schema.dump(clients).data

        return {'status': 'ok', 'clients': clients, 'n_rows': n_rows}

    def patch(self, client_id, token=''):
        if access(db, models, '/acl/clients', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        clients = db.session.query(models.Clients)
        clients = clients.filter(models.Clients.id == client_id)

        clients.update(args)
        db.session.commit()
        n_rows = clients.count()

        clients = get_clients(db, models)
        clients = clients.filter(models.Clients.id == client_id)

        clients_schema = models.ClientSchema(many=True)
        clients = clients_schema.dump(clients).data

        return {'n_rows': n_rows, 'clients': clients}
