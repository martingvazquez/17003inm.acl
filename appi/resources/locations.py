from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access, get_locations
import uuid


class Locations(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/locations', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted', required=True)
        parser.add_argument('cp', type=str, help='CP cannot be converted', required=True)
        parser.add_argument('street', type=str, help='Street cannot be converted', required=True)
        parser.add_argument('colony', type=str, help='Colony cannot be converted', required=True)
        parser.add_argument('street_address', type=str, help='Street_address cannot be converted', required=True)
        parser.add_argument('apartment_number', type=str, help='Apartment_number cannot be converted', required=True)
        parser.add_argument('lot', type=str, help='Lot cannot be converted', required=True)
        parser.add_argument('block', type=str, help='Block cannot be converted', required=True)
        parser.add_argument('project_id', type=str, help='Proyect_id cannot be converted', required=True)

        args = parser.parse_args()

        description = args['description']
        notes = args['notes']
        cp = args['cp']
        street = args['street']
        colony = args['colony']
        street_address = args['street_address']
        apartment_number = args['apartment_number']
        lot = args['lot']
        block = args['block']
        project_id = args['project_id']

        locations = models.Locations(description=description, notes=notes, cp=cp, street=street, colony=colony, street_address=street_address,
                                   apartment_number=apartment_number, lot=lot, block=block,uuid=uuid.uuid4(), insert_user_uuid=uuid.uuid4(),
                                   update_user_uuid=uuid.uuid4(), company_uuid=uuid.uuid4(), project_id=project_id)

        try:
            db.session.add(locations)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        location_id = locations.id
        locations = get_locations(db, models)
        locations = locations.filter(models.Locations.id == location_id)

        locations_schema = models.LocationSchema(many=True)
        locations = locations_schema.dump(locations).data

        return {'status': 'ok','locations': locations}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,location_id=-1, all_show=0, column=''):
        if access(db, models, '/acl/locations', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        locations = get_locations(db, models)
        print(locations)
        print("000000000000000")
        locations_load = {}

        if location_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                locations = locations.order_by(order)
                locations = locations.filter(
                    or_(
                        models.Locations.id == condition_int,
                        models.Locations.description.ilike(condition_string),
                        models.Locations.notes.ilike(condition_string),
                        models.Locations.cp.ilike(condition_string),
                        models.Locations.street.ilike(condition_string),
                        models.Locations.colony.ilike(condition_string),
                        models.Locations.street_address.ilike(condition_string),
                        models.Locations.apartment_number.ilike(condition_string),
                        models.Locations.lot.ilike(condition_string),
                        models.Locations.block.ilike(condition_string),
                        models.Project_types.description.ilike(condition_string)
                        # models.Locations.status == condition_int
                    )
                )

                n_rows = locations.count()

                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    locations = locations.limit(int(items_page)).offset(n_rows_show)
                locations_load['n_rows'] = n_rows
            else:
                if column == 'locations.status':
                    locations = locations.filter(models.Locations.status == data_filter)
                    n_rows = locations.count()
                    locations_load['n_rows'] = n_rows
        else:
            if location_id >= 1:
                locations = locations.filter(models.Locations.id == location_id)
                n_rows = locations.count()
                locations_load['n_rows'] = n_rows

        print(locations_load)
        print("3333333333333333")
        locations_schema = models.LocationSchema(many=True)

        print(locations)
        print("11111")
        locations = locations_schema.dump(locations).data
        print(locations)

        return {'locations':locations, 'n_rows':n_rows}

    def put(self, location_id, token=''):
        if access(db, models, '/acl/locations', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        locations_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('cp', type=str, help='CP cannot be converted', required=True)
        parser.add_argument('street', type=str, help='Street cannot be converted', required=True)
        parser.add_argument('colony', type=str, help='Colony cannot be converted', required=True)
        parser.add_argument('street_address', type=str, help='Street_address cannot be converted', required=True)
        parser.add_argument('apartment_number', type=str, help='Apartment_number cannot be converted', required=True)
        parser.add_argument('lot', type=str, help='Lot cannot be converted', required=True)
        parser.add_argument('block', type=str, help='Block cannot be converted', required=True)
        parser.add_argument('project_id', type=str, help='Proyect_id cannot be converted')
        args = parser.parse_args()

        locations = db.session.query(models.Locations)
        locations = locations.filter(models.Locations.id == location_id)

        n_rows = locations.count()

        try:
            locations.update(args)
            db.session.commit()
            locations = get_locations(db, models)
            locations = locations.filter(models.Locations.id == location_id)
        except Exception as e:
            print(e)
            db.session.rollback()
            locations_schema = models.LocationSchema(many=True)
            locations = locations_schema.dump(locations)
            locations_load['locations'] = locations.data
            locations_load['n_rows'] = n_rows
            locations_load['status'] = 'false'
            return locations_load

        locations_schema = models.LocationSchema(many=True)
        locations = locations_schema.dump(locations).data

        return {'status': 'ok', 'locations': locations, 'n_rows': n_rows}

    def patch(self, location_id, token=''):
        if access(db, models, '/acl/locations', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        locations = db.session.query(models.Locations)
        locations = locations.filter(models.Locations.id == location_id)

        locations.update(args)
        db.session.commit()
        n_rows = locations.count()

        locations = get_locations(db, models)
        locations = locations.filter(models.Locations.id == location_id)

        locations_schema = models.LocationSchema(many=True)
        locations = locations_schema.dump(locations).data

        return {'n_rows': n_rows, 'locations': locations}
