from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access, upper, language
import uuid
from datetime import datetime, timedelta


class Countries(Resource):
    def post(self, token):
        if access(db, models, '/acl/countries', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        countries_schema = models.CountrySchema()
        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', type=str, help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            args = parser.parse_args()

            description = args['description']
            notes = args['notes']

            if upper == 1:
                description = description.upper()
                if notes is not None:
                    notes = notes.upper()


            countries = models.Countries(description=description, notes=notes, uuid=uuid.uuid4())
            try:
                db.session.add(countries)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                if language == 1:
                    return {'error': '5', 'message': 'Error descripción repetida'}
                else:
                    return {'error': '5', 'message': 'Error description repeat'}

            countries = countries_schema.dump(countries)
            return {'status': 'ok', 'countries': countries}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406


    def get(self, token,column_order_by='', type_order='', data_filter='', 
            current_page=1, items_page=5, country_id=-1, show_all=0, column=''):
        if access(db, models, '/acl/countries', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        countries = db.session.query(models.Countries)
        countries_load = {}

        if country_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                countries = countries.order_by(order)
                countries = countries.filter(
                    or_(
                        models.Countries.id == condition_int,
                        models.Countries.description.ilike(condition_string),
                        models.Countries.notes.ilike(condition_string)
                    )
                )
                n_rows = countries.count()
                if show_all==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    countries = countries.limit(int(items_page)).offset(n_rows_show)
                countries_load['n_rows'] = n_rows
            else:
                if column == 'countries.status':
                    countries = countries.filter(models.Countries.status == data_filter)
                    n_rows = countries.count()
                    countries_load['n_rows'] = n_rows

        else:
            if country_id >= 1:
                countries = countries.filter(models.Countries.id == country_id)
                n_rows = countries.count()
                countries_load['n_rows'] = n_rows

        countries_schema = models.CountrySchema(many=True)
        countries = countries_schema.dump(countries)
        countries_load['countries'] = countries.data
        return countries_load

    def put(self,country_id, token=''):
        if access(db, models, '/acl/countries', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        countries_load = {}
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            args = parser.parse_args()

            #if upper == 1:
            #print(args.description.upper())
            #print(args.notes.upper())
            #args.description.upper()
            #args.notes.upper()
            #print(args)

            countries = db.session.query(models.Countries)
            countries = countries.filter(models.Countries.id == country_id)

            n_rows = countries.count()

            try:
                countries.update(args)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                countries_schema = models.CountrySchema(many=True)
                countries = countries_schema.dump(countries)
                countries_load['countries'] = countries.data
                countries_load['n_rows'] = n_rows
                countries_load['status'] = 'false'
                return countries_load

            countries_schema = models.CountrySchema(many=True)
            countries = countries_schema.dump(countries)
            countries_load['countries'] = countries.data
            countries_load['n_rows'] = n_rows
            countries_load['status'] = 'ok'

            return countries_load
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def patch(self, country_id, token):
        if access(db, models, '/acl/countries', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        countries_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        countries = db.session.query(models.Countries)
        countries = countries.filter(models.Countries.id == country_id)

        countries.update(args)
        db.session.commit()

        n_rows = countries.count()
        countries_schema = models.CountrySchema(many=True)
        countries = countries_schema.dump(countries)
        countries_load['countries'] = countries.data
        countries_load['n_rows'] = n_rows
        return countries_load
