from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access
import uuid


class DocumentTypes(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/document_types', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='Description cannot be converted')
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()
        
        description = args['description']
        notes = args['notes']

        document_types = models.Document_types(description = description, notes=notes, uuid=uuid.uuid4(), insert_user_uuid=uuid.uuid4(),
                                 update_user_uuid=uuid.uuid4(), company_uuid=uuid.uuid4())
        try:
            db.session.add(document_types)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        document_types = db.session.query(models.Document_types)
        document_types_schema = models.DocumentTypeSchema(many=True)
        document_types = document_types_schema.dump(document_types)
        return {'status': 'ok', 'document_types': document_types}
 
    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,document_type_id=-1,
            all_show=0, column='', column2='', data_filter2=''):
        if access(db, models, '/acl/document_types', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        document_types = db.session.query(models.Document_types)
        document_types_load = {}

        if document_type_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                document_types = document_types.order_by(order)
                document_types = document_types.filter(
                    or_(
                        models.Document_types.id == condition_int,
                        models.Document_types.description.ilike(condition_string),
                        models.Document_types.notes.ilike(condition_string)
                        # models.Document_types.status == condition_int
                    )
                )

                n_rows = document_types.count()

                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    document_types = document_types.limit(int(items_page)).offset(n_rows_show)
                document_types_load['n_rows'] = n_rows
            else:
                if column == 'document_types.status':
                    document_types = document_types.filter(models.Document_types.status == data_filter)
                    n_rows = document_types.count()
                    document_types_load['n_rows'] = n_rows
        else:
            if document_type_id >= 1:
                document_types = document_types.filter(models.Document_types.id == document_type_id)
                n_rows = document_types.count()
                document_types_load['n_rows'] = n_rows


        document_types_schema = models.DocumentTypeSchema(many=True)
        document_types = document_types_schema.dump(document_types)
        document_types_load['document_types'] = document_types.data

        return document_types_load

    def put(self, document_type_id, token=''):
        if access(db, models, '/acl/document_types', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        document_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()

        document_types = db.session.query(models.Document_types)
        document_types = document_types.filter(models.Document_types.id == document_type_id)

        n_rows = document_types.count()

        try:
            document_types.update(args)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            document_types_schema = models.DocumentTypeSchema(many=True)
            document_types = document_types_schema.dump(document_types)
            document_types_load['document_types'] = document_types.data
            document_types_load['n_rows'] = n_rows
            document_types_load['status'] = 'false'
            return document_types_load

        document_types_schema = models.DocumentTypeSchema(many=True)
        document_types = document_types_schema.dump(document_types)
        document_types_load['document_types'] = document_types.data
        document_types_load['n_rows'] = n_rows
        document_types_load['status'] = 'ok'
        return document_types_load

    def patch(self,document_type_id, token=''):
        if access(db, models, '/acl/document_types', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        document_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        document_types = db.session.query(models.Document_types)
        document_types = document_types.filter(models.Document_types.id == document_type_id)

        document_types.update(args)
        db.session.commit()
        n_rows = document_types.count()

        document_types_schema = models.DocumentTypeSchema(many=True)
        document_types = document_types_schema.dump(document_types)
        
        document_types_load['document_types'] = document_types.data
        document_types_load['n_rows'] = n_rows
        
        return document_types_load
