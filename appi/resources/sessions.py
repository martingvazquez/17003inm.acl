from flask_restful import Resource
from application import models, db, get_users, get_tokens, review_token
import hashlib
from datetime import datetime


class Sessions(Resource):
    def get(self, user='', password=''):
        review_token(db, models)
        users = get_users(db, models)
        users = users.filter(models.Users.user.like(user))
        users = users.filter(models.Users.password.like(password))
        n_rows = users.count()
        if n_rows <= 0:
            return {'error': '1'}

        else:
            users_schema = models.UserSchema(many=True)
            users = users_schema.dump(users).data
            for user in users:
                group_uuid = user['group_uuid']
                user_uuid = user['user_uuid']
                tokens = get_tokens(db, models)
                tokens = tokens.filter(models.Tokens.user_uuid == user_uuid)
                n_tokens = tokens.count()
                if n_tokens >= 1:
                    return {'error': '2'}
                else:
                    now = datetime.now()
                    str_token = group_uuid+user_uuid+str(now)
                    token = hashlib.md5(str_token.encode())
                    token = token.hexdigest()
                    tokens = models.Tokens(group_uuid=group_uuid, user_uuid=user_uuid, token=token)
                    db.session.add(tokens)
                    db.session.commit()
            return {'error': '0', 'token': token}

    def delete(self, token='moro58'):
        if token != 'moro58':
            query = db.session.query(models.Tokens)
            query = query.filter(models.Tokens.token == token)
            n_tokens = query.count()
            if n_tokens >= 1:
                tokens_delete = query.one()
                db.session.delete(tokens_delete)
                db.session.commit()
                return {'error': '0', 'message': 'Se eliminó la sesion'}
            else:
                return {'error': '0', 'message': 'No hay sesiones por eliminar'}
        else:
            query = db.session.query(models.Tokens)
            n_tokens = query.count()
            i = 0
            if n_tokens == 1:
                tokens_delete = query.one()
                db.session.delete(tokens_delete)
                db.session.commit()
