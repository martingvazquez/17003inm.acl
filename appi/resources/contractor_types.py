from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access
import uuid


class ContractorTypes(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/contractor_types', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='Description cannot be converted')
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()
        
        description = args['description']
        notes = args['notes']

        contractor_types = models.Contractor_types(description = description, notes=notes, uuid=uuid.uuid4(), insert_user_uuid=uuid.uuid4(),
                                 update_user_uuid=uuid.uuid4(), company_uuid=uuid.uuid4())
        try:
            db.session.add(contractor_types)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        contractor_types = db.session.query(models.Contractor_types)
        contractor_types_schema = models.ContractorTypeSchema(many=True)
        contractor_types = contractor_types_schema.dump(contractor_types)
        return {'status': 'ok', 'contractor_types': contractor_types}
 
    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,contractor_type_id=-1,
            all_show=0, column='', column2='', data_filter2=''):
        if access(db, models, '/acl/contractor_types', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        contractor_types = db.session.query(models.Contractor_types)
        contractor_typesLoad = {}

        if contractor_type_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                contractor_types = contractor_types.order_by(order)
                contractor_types = contractor_types.filter(
                    or_(
                        models.Contractor_types.id == condition_int,
                        models.Contractor_types.description.ilike(condition_string),
                        models.Contractor_types.notes.ilike(condition_string)
                        # models.Contractor_types.status == condition_int
                    )
                )

                n_rows = contractor_types.count()

                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    contractor_types = contractor_types.limit(int(items_page)).offset(n_rows_show)
                contractor_typesLoad['n_rows'] = n_rows
            else:
                if column == 'contractor_types.status':
                    contractor_types = contractor_types.filter(models.Contractor_types.status == data_filter)
                    n_rows = contractor_types.count()
                    contractor_typesLoad['n_rows'] = n_rows
        else:
            if contractor_type_id >= 1:
                contractor_types = contractor_types.filter(models.Contractor_types.id == contractor_type_id)
                n_rows = contractor_types.count()
                contractor_typesLoad['n_rows'] = n_rows


        contractor_types_schema = models.ContractorTypeSchema(many=True)
        contractor_types = contractor_types_schema.dump(contractor_types)
        contractor_typesLoad['contractor_types'] = contractor_types.data

        return contractor_typesLoad

    def put(self, contractor_type_id, token=''):
        if access(db, models, '/acl/contractor_types', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        contractor_typesLoad = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()

        contractor_types = db.session.query(models.Contractor_types)
        contractor_types = contractor_types.filter(models.Contractor_types.id == contractor_type_id)

        n_rows = contractor_types.count()

        try:
            contractor_types.update(args)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            contractor_types_schema = models.ContractorTypeSchema(many=True)
            contractor_types = contractor_types_schema.dump(contractor_types)
            contractor_typesLoad['contractor_types'] = contractor_types.data
            contractor_typesLoad['n_rows'] = n_rows
            contractor_typesLoad['status'] = 'false'
            return contractor_typesLoad

        contractor_types_schema = models.ContractorTypeSchema(many=True)
        contractor_types = contractor_types_schema.dump(contractor_types)
        contractor_typesLoad['contractor_types'] = contractor_types.data
        contractor_typesLoad['n_rows'] = n_rows
        contractor_typesLoad['status'] = 'ok'
        return contractor_typesLoad

    def patch(self, contractor_type_id, token=''):
        if access(db, models, '/acl/contractor_types', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        contractor_typesLoad = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        contractor_types = db.session.query(models.Contractor_types)
        contractor_types = contractor_types.filter(models.Contractor_types.id == contractor_type_id)

        contractor_types.update(args)
        db.session.commit()
        n_rows = contractor_types.count()

        contractor_types_schema = models.ClientTypeSchema(many=True)
        contractor_types = contractor_types_schema.dump(contractor_types)
        
        contractor_typesLoad['contractor_types'] = contractor_types.data
        contractor_typesLoad['n_rows'] = n_rows
        
        return contractor_typesLoad
