from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, \
    get_groups, access, upper, language
import uuid


class Groups(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/groups', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description',  help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            args = parser.parse_args()

            description = args['description']
            notes = args['notes']

            if upper == 1:
                description = description.upper()
                if notes is not None:
                    notes = notes.upper()

            groups = models.Groups(description=description, notes=notes, uuid=uuid.uuid4())
            try:
                db.session.add(groups)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                if language == 1:
                    return {'error': '5', 'message': 'Error descripción repetida'}
                else:
                    return {'error': '5', 'message': 'Error description repeat'}

            group_id = groups.id
            groups = get_groups(db, models)
            groups = groups.filter(models.Groups.id == group_id)
            groups_schema = models.GroupSchema(many=True)
            groups = groups_schema.dump(groups).data
            return {'status': 'ok', 'groups': groups}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406


    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1,
            items_page=5, group_id=-1, show_all=0, column='', permission=0):
        if access(db, models, '/acl/groups', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        groups = get_groups(db, models)
        groups_load = {}

        if permission == 0:
            if group_id == -1:
                if column == '':
                    order = get_order_by(column_order_by, type_order)
                    data_filter_integer, data_filter_string = get_data_filter(data_filter)

                    condition_string = '%'+data_filter_string+'%'
                    condition_int = data_filter_integer

                    groups = groups.order_by(order)
                    groups = groups.filter(
                        or_(
                            models.Groups.id == condition_int,
                            models.Groups.description.ilike(condition_string),
                            models.Groups.notes.ilike(condition_string)
                        )
                    )
                    n_rows = groups.count()

                    if show_all == 0:
                        n_rows_show = (int(current_page)-1) * int(items_page)
                        groups = groups.limit(int(items_page)).offset(n_rows_show)
                    groups_load['n_rows'] = n_rows
                else:
                    if column == 'groups.status':
                        groups = groups.filter(models.Groups.status == data_filter)
                        n_rows = groups.count()
                        groups_load['n_rows'] = n_rows

            else:
                if group_id >= 1:
                    groups = groups.filter(models.Groups.id == group_id)
                    n_rows = groups.count()
                    groups_load['n_rows'] = n_rows

            groups_schema = models.GroupSchema(many=True)
            groups = groups_schema.dump(groups)

            groups_load['groups'] = groups.data

            return groups_load
        else:
            permissions = {}

            groups = get_groups(db, models)
            groups = groups.filter(models.Groups.id == group_id)
            groups = groups.filter(models.Groups.status == 1)
            groups = groups.first()

            group = groups.description
            group_id = groups.id
            group_status = groups.status

            applications = db.session.query(models.Applications)
            applications = applications.filter(models.Applications.status == 1)
            applications = applications.all()

            applications_show = []
            index = 0
            for application in applications:
                application_json = {}
                application_json['application_id'] = application.id
                application_json['application'] = application.description
                application_json['status'] = application.status

                application_id = application.id

                resources = db.session.query(models.Resources)
                resources = resources.filter(models.Resources.application_id == application_id)
                resources = resources.filter(models.Resources.status == 1)

                resources_show = []
                for resource in resources:
                    resource_json = {}
                    resource_json['resource_id'] = resource.id
                    resource_json['resource'] = resource.description
                    resource_json['status'] = resource.status

                    resource_id = resource.id

                    urls = db.session.query(models.Urls)
                    urls = urls.filter(models.Urls.resource_id == resource_id)
                    urls = urls.filter(models.Urls.status == 1)
                    urls = urls.all()

                    urls_show = []
                    for url in urls:
                        url_json = {}
                        url_json['url_id'] = url.id
                        url_json['url'] = url.description
                        url_json['status'] = url.status
                        url_json['index'] = index
                        url_json['method'] = url.method
                        url_id = url.id

                        urls_groups = db.session.query(models.UrlsGroups)
                        urls_groups = urls_groups.filter(models.UrlsGroups.url_id == url_id)
                        urls_groups = urls_groups.filter(models.UrlsGroups.group_id == group_id)

                        n_urls_groups = urls_groups.count()

                        if n_urls_groups == 1:
                            existe = True
                        else:
                            existe = False

                        url_json['selected'] = existe

                        index = index + 1
                        urls_show.append(url_json)

                    resource_json['urls'] = urls_show
                    resources_show.append(resource_json)

                application_json['resources'] = resources_show
                applications_show.append(application_json)

            permissions['group'] = group
            permissions['group_id'] = group_id
            permissions['group_status'] = group_status
            permissions['applications'] = applications_show

            return permissions

    def put(self, group_id, token=''):
        if access(db, models, '/acl/groups', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        groups_load = {}
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            args = parser.parse_args()
            groups = db.session.query(models.Groups)
            groups = groups.filter(models.Groups.id == group_id)
            n_rows = groups.count()
            try:
                groups.update(args)
                db.session.commit()
                groups = get_groups(db, models)
                groups = groups.filter(models.Groups.id == group_id)
            except Exception as e:
                print(e)
                db.session.rollback()
                groups_schema = models.StateSchema(many=True)
                groups = groups_schema.dump(groups)
                groups_load['groups'] = groups.data
                groups_load['n_rows'] = n_rows
                groups_load['status'] = 'false'
                return groups_load

            groups_schema = models.GroupSchema(many=True)
            groups = groups_schema.dump(groups).data
            return {'status': 'ok', 'groups': groups, 'n_rows': n_rows}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def patch(self, group_id, token=''):
        if access(db, models, '/acl/groups', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        groups = db.session.query(models.Groups)
        groups = groups.filter(models.Groups.id == group_id)

        groups.update(args)
        db.session.commit()

        n_rows = groups.count()

        groups = get_groups(db, models)
        groups = groups.filter(models.Groups.id == group_id)

        groups_schema = models.GroupSchema(many=True)
        groups = groups_schema.dump(groups).data

        return {'n_rows': n_rows, 'groups': groups}
