from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access, get_documents
import uuid


class Documents(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/documents', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted', required=True)
        parser.add_argument('file', type=str, help='File cannot be converted', required=True)
        parser.add_argument('document_type_id', type=str, help='Document_type_id cannot be converted', required=True)
        args = parser.parse_args()
        description = args['description']
        notes = args['notes']
        file = args['file']
        document_type_id = args['document_type_id']

        documents = models.Documents(description=description, notes=notes, file=file, uuid=uuid.uuid4(),
                                     insert_user_uuid=uuid.uuid4(), update_user_uuid=uuid.uuid4(),
                                     company_uuid=uuid.uuid4(), document_type_id=document_type_id)

        print("documents")
        try:
            db.session.add(documents)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        document_id = documents.id
        documents = get_documents(db, models)
        documents = documents.filter(models.Documents.id == document_id)

        documents_schema = models.DocumentSchema(many=True)
        documents = documents_schema.dump(documents).data

        return {'status': 'ok','documents': documents}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,document_id=-1, all_show=0,
            column='', column2='', data_filter2=''):
        if access(db, models, '/acl/documents', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        documents = get_documents(db, models)
        documents_load = {}

        if document_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                documents = documents.order_by(order)
                documents = documents.filter(
                    or_(
                        models.Documents.id == condition_int,
                        models.Documents.description.ilike(condition_string),
                        models.Documents.notes.ilike(condition_string),
                        models.Documents.file.ilike(condition_string),
                        models.Document_types.description.ilike(condition_string)
                        # models.Documents.status == condition_int
                    )
                )

                n_rows = documents.count()

                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    documents = documents.limit(int(items_page)).offset(n_rows_show)
                documents_load['n_rows'] = n_rows
            else:
                if column == 'documents.status':
                    documents = documents.filter(models.Documents.status == data_filter)
                    n_rows = documents.count()
                    documents_load['n_rows'] = n_rows
        else:
            if document_id >= 1:
                documents = documents.filter(models.Documents.id == document_id)
                n_rows = documents.count()
                documents_load['n_rows'] = n_rows


        documents_schema = models.DocumentSchema(many=True)
        documents = documents_schema.dump(documents).data
        print(documents)

        return {'documents':documents, 'n_rows':n_rows}

    def put(self, document_id, token=''):
        if access(db, models, '/acl/documents', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        documents_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('file', type=str, help='file cannot be converted')
        parser.add_argument('document_type_id', type=str, help='Document_type_id cannot be converted')
        args = parser.parse_args()

        documents = db.session.query(models.Documents)
        documents = documents.filter(models.Documents.id == document_id)

        n_rows = documents.count()

        try:
            documents.update(args)
            db.session.commit()
            documents = get_documents(db, models)
            documents = documents.filter(models.Documents.id == document_id)
        except Exception as e:
            print(e)
            db.session.rollback()
            documents_schema = models.DocumentSchema(many=True)
            documents = documents_schema.dump(documents)
            documents_load['documents'] = documents.data
            documents_load['n_rows'] = n_rows
            documents_load['status'] = 'false'
            return documents_load

        documents_schema = models.DocumentSchema(many=True)
        documents = documents_schema.dump(documents).data

        return {'status': 'ok', 'documents': documents, 'n_rows': n_rows}

    def patch(self, document_id, token=''):
        if access(db, models, '/acl/documents', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        documents = db.session.query(models.Documents)
        documents = documents.filter(models.Documents.id == document_id)

        documents.update(args)
        db.session.commit()
        n_rows = documents.count()

        documents = get_documents(db, models)
        documents = documents.filter(models.Documents.id == document_id)

        documents_schema = models.DocumentSchema(many=True)
        documents = documents_schema.dump(documents).data

        return {'n_rows': n_rows, 'documents': documents}
