from flask_restful import Resource
from application import models, db, get_users, get_tokens, access_external_application, review_token
import hashlib
from datetime import datetime

class Tokens(Resource):

    def delete(self, other_application=0):
        if other_application == 1:
            review_token(db, models)
            return {'message': 'Accepted'}, 202
        else:
            return {'error': '405', 'message': 'Method not allowed'}, 405

    def get(self, token='', application='', resource='', method=''):
        #print(token)
        permission = access_external_application(db, models, application, resource, method, token)
        if permission == 1:
            return {'message': 'OK'}, 200
        else:
            return {'error': '401', 'message': 'Unauthorized '}, 401
