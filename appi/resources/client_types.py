from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access
import uuid


class ClientTypes(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/client_types', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description',  help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()

        description = args['description']
        notes = args['notes']

        client_types = models.ClientTypes(description=description, notes=notes, uuid=uuid.uuid4(),
                                          insert_user_uuid=uuid.uuid4(), update_user_uuid=uuid.uuid4(),
                                          company_uuid=uuid.uuid4())
        try:
            db.session.add(client_types)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}
        client_types = db.session.query(models.ClientTypes)
        client_types_schema = models.ClientTypeSchema(many=True)
        client_types = client_types_schema.dump(client_types)
        return {'status': 'ok', 'client_types': client_types}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5,
            client_type_id=-1, all_show=0, column=''):
        if access(db, models, '/acl/client_types', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        client_types = db.session.query(models.ClientTypes)
        client_types_load = {}

        if client_type_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                client_types = client_types.order_by(order)
                client_types = client_types.filter(
                    or_(
                        models.ClientTypes.id == condition_int,
                        models.ClientTypes.description.ilike(condition_string),
                        models.ClientTypes.notes.ilike(condition_string)
                    )
                )
                n_rows = client_types.count()
                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    client_types = client_types.limit(int(items_page)).offset(n_rows_show)
                client_types_load['n_rows'] = n_rows
            else:
                if column == 'client_types.status':
                    client_types = client_types.filter(models.ClientTypes.status == data_filter)
                    n_rows = client_types.count()
                    client_types_load['n_rows'] = n_rows
        else:
            if client_type_id >= 1:
                client_types = client_types.filter(models.ClientTypes.id == client_type_id)
                n_rows = client_types.count()
                client_types_load['n_rows'] = n_rows

        client_types_schema = models.ClientTypeSchema(many=True)
        client_types = client_types_schema.dump(client_types)
        client_types_load['client_types'] = client_types.data

        print(client_types_load)

        return client_types_load

    def put(self, client_type_id, token=''):
        if access(db, models, '/acl/client_types', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        client_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')

        args = parser.parse_args()

        client_types = db.session.query(models.ClientTypes)
        client_types = client_types.filter(models.ClientTypes.id == client_type_id)
        n_rows = client_types.count()

        try:
            client_types.update(args)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            client_types_schema = models.ClientTypeSchema(many=True)
            client_types = client_types_schema.dump(client_types)
            client_types_load['client_types'] = client_types.data
            client_types_load['n_rows'] = n_rows
            client_types_load['status'] = 'false'
            return client_types_load

        client_types_schema = models.ClientTypeSchema(many=True)
        client_types = client_types_schema.dump(client_types)
        client_types_load['client_types'] = client_types.data
        client_types_load['n_rows'] = n_rows
        client_types_load['status'] = 'ok'
        return client_types_load

    def patch(self, client_type_id, token=''):
        if access(db, models, '/acl/client_types', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        client_types_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        client_types = db.session.query(models.ClientTypes)
        client_types = client_types.filter(models.ClientTypes.id == client_type_id)

        client_types.update(args)
        db.session.commit()
        n_rows = client_types.count()

        client_types_schema = models.ClientTypeSchema(many=True)
        client_types = client_types_schema.dump(client_types)

        client_types_load['client_types'] = client_types.data
        client_types_load['n_rows'] = n_rows

        return client_types_load
