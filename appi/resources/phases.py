from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access
import uuid


class Phases(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/phases', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='Description cannot be converted')
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()
        
        description = args['description']
        notes = args['notes']

        phases = models.Phases(description=description, notes=notes, uuid=uuid.uuid4(), insert_user_uuid=uuid.uuid4(),
                                 update_user_uuid=uuid.uuid4(), company_uuid=uuid.uuid4())
        try:
            db.session.add(phases)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        phases = db.session.query(models.Phases)
        phases_schema = models.PhaseSchema(many=True)
        phases = phases_schema.dump(phases)

        return {'status': 'ok', 'phases': phases}
 
    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5, phase_id=-1, all_show=0, column='',
            column2='', data_filter2=''):
        if access(db, models, '/acl/phases', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        phases = db.session.query(models.Phases)
        phases_load = {}

        if phase_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                phases = phases.order_by(order)
                phases = phases.filter(
                    or_(
                        models.Phases.id == condition_int,
                        models.Phases.description.ilike(condition_string),
                        models.Phases.notes.ilike(condition_string)
                        # models.Phases.status == condition_int
                    )
                )

                n_rows = phases.count()

                if all_show==0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    phases = phases.limit(int(items_page)).offset(n_rows_show)
                phases_load['n_rows'] = n_rows
            else:
                if column == 'phases.status':
                    phases = phases.filter(models.Phases.status == data_filter)
                    n_rows = phases.count()
                    phases_load['n_rows'] = n_rows
        else:
            if phase_id >= 1:
                phases = phases.filter(models.Phases.id == phase_id)
                n_rows = phases.count()
                phases_load['n_rows'] = n_rows

        phases_schema = models.PhaseSchema(many=True)
        phases = phases_schema.dump(phases)
        phases_load['phases'] = phases.data

        return phases_load

    def put(self, phase_id, token=''):
        if access('put',  token, 'inmobiliaria', 'phases') == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        phases_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='Description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        args = parser.parse_args()

        phases = db.session.query(models.Phases)
        phases = phases.filter(models.Phases.id == phase_id)

        n_rows = phases.count()

        try:
            phases.update(args)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            phases_schema = models.PhaseSchema(many=True)
            phases = phases_schema.dump(phases)
            phases_load['phases'] = phases.data
            phases_load['n_rows'] = n_rows
            phases_load['status'] = 'false'
            return phases_load

        phases_schema = models.PhaseSchema(many=True)
        phases = phases_schema.dump(phases)
        phases_load['phases'] = phases.data
        phases_load['n_rows'] = n_rows
        phases_load['status'] = 'ok'
        return phases_load

    def patch(self, phase_id, token=''):
        if access('patch',  token, 'inmobiliaria', 'phases') == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        phases_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        phases = db.session.query(models.Phases)
        phases = phases.filter(models.Phases.id == phase_id)

        phases.update(args)
        db.session.commit()
        n_rows = phases.count()

        phases_schema = models.ClientTypeSchema(many=True)
        phases = phases_schema.dump(phases)
        
        phases_load['phases'] = phases.data
        phases_load['n_rows'] = n_rows
        
        return phases_load
