from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, func
import uuid

def elimina(url_id,group_id):
    query = db.session.query(models.UrlsGroups)
    query = query.filter(models.UrlsGroups.group_id == group_id)
    query = query.filter(models.UrlsGroups.url_id == url_id)
    urls_groups = query.one()
    db.session.delete(urls_groups)
    urls_groups = query.first()
    db.session.commit()
    return urls_groups

def alta(url_id,group_id):
    urls_groups = models.UrlsGroups(url_id=url_id, group_id = group_id)
    db.session.add(urls_groups)
    db.session.commit()
    return urls_groups

class Groups_Urls(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('group_id', required=True)
        parser.add_argument('urls_id',  action='applicationnd')
        args = parser.parse_args()

        group_id = args.group_id
        urls_id = args.urls_id

        query = db.session.query(models.UrlsGroups)
        query = query.filter(models.UrlsGroups.group_id == group_id)
        urls_groups = query.all()

        for url_group in urls_groups:
            elimina(url_group.url_id,group_id)

        if urls_id != None:
            for url_id in urls_id:
                query = db.session.query(models.UrlsGroups)
                query = query.filter(models.UrlsGroups.group_id == group_id)
                query = query.filter(models.UrlsGroups.url_id == url_id)
                n_urls_groups = query.count()
                if n_urls_groups == 0:
                    alta(url_id,group_id)

        return {'status':'ok'}

    def get(self,group_id):
        urls = db.session.query(
            models.Urls.id.label('url_id'),
            models.Groups.id.label('group_id'),
            models.Urls.description.label('url'),
            models.Urls.status.label('url_status'),
            models.Groups.description.label('group'),
            models.Groups.status.label('group_status')
        )
        urls = urls.filter(models.Urls.status==1)
        urls = urls.filter(models.Groups.id == group_id)

        n_rows = urls.count()
        urls_schema = models.UrlGroupSchema(many=True)
        urls = urls_schema.dump(urls).data

        i = 0
        for url in urls:
            url_id = url['url_id']
            urls_groups = db.session.query(models.UrlsGroups)
            urls_groups = urls_groups.filter(models.UrlsGroups.url_id == url_id)
            urls_groups = urls_groups.filter(models.UrlsGroups.group_id == group_id)
            nUrls = urls_groups.count()
            urls[i]['existe'] = nUrls
            i = i+1

        return {'urls': urls, 'n_rows': n_rows}