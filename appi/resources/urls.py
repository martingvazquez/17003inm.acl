from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, get_urls, access
import uuid


class Urls(Resource):

    def post(self, token):
        if access(db, models, '/acl/urls', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('resource_id', type=int, help='Notes cannot be converted', required=True)
            parser.add_argument('method', type=str, help='Notes cannot be converted', required=True)
            args = parser.parse_args()

            description = args['description']
            notes = args['notes']
            resource_id = args['resource_id']
            method = args['method']

            urls = models.Urls(description=description, notes=notes,resource_id = resource_id,method=method, uuid=uuid.uuid4())
            try:
                db.session.add(urls)
                db.session.commit()
            except Exception as e:
                print(e)
                db.session.rollback()
                return {'status': 'false'}

            url_id = urls.id
            urls = get_urls(db, models)
            urls = urls.filter(models.Urls.id == url_id)

            urls_schema = models.UrlSchema(many=True)
            urls = urls_schema.dump(urls).data

            return {'status': 'ok', 'urls': urls}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406


    def get(self, token, column_order_by='', type_order='', data_filter='', current_page=1, items_page=5, url_id=-1, show_all=0,column='', column2='', data_filter2=''):

        if access(db, models, '/acl/urls', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        urls = get_urls(db, models)
        urls_load = {}
        if url_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%' + data_filter_string + '%'
                condition_int = data_filter_integer

                urls = urls.order_by(order)
                urls = urls.filter(
                    or_(
                        models.Urls.id == condition_int,
                        models.Urls.description.ilike(condition_string),
                        models.Urls.notes.ilike(condition_string),
                        models.Resources.description.ilike(condition_string),
                        models.Applications.description.ilike(condition_string),
                        models.Urls.method.ilike(condition_string)
                    )
                )
                n_rows = urls.count()
                if show_all == 0:
                    n_rows_show = (int(current_page) - 1) * int(items_page)
                    urls = urls.limit(int(items_page)).offset(n_rows_show)
                urls_load['n_rows'] = n_rows
            else:
                if column == 'urls.status':
                    urls = urls.filter(models.Urls.status == data_filter)

                if column == 'urls.resource_id':
                    urls = urls.filter(models.Urls.resource_id == data_filter)
                if column2 != '':
                    if column2 == 'urls.status':
                        urls = urls.filter(models.Urls.status == data_filter2 )

                n_rows = urls.count()
                urls_load['n_rows'] = n_rows
        else:
            if url_id >= 1:
                urls = urls.filter(models.Urls.id == url_id)
                n_rows = urls.count()
                urls_load['n_rows'] = n_rows

        urls_schema = models.UrlSchema(many=True)
        urls = urls_schema.dump(urls).data

        return {'urls': urls, 'n_rows': n_rows}

    def put(self, url_id, token=''):
        if access(db, models, '/acl/urls', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str, help='Notes cannot be converted')
            parser.add_argument('resource_id',  help='State', required=True)
            parser.add_argument('method', type=str, help='Method', required=True)

            args = parser.parse_args()
            urls = db.session.query(models.Urls)
            urls = urls.filter(models.Urls.id == url_id)
            n_rows = urls.count()

            try:
                urls.update(args)
                db.session.commit()
                urls = get_urls(db, models)
                urls = urls.filter(models.Urls.id == url_id)
            except Exception as e:
                print(e)
                db.session.rollback()
                urls = get_urls(db, models)
                urls = urls.filter(models.Urls.id == url_id)
                urls_schema = models.UrlSchema(many=True)
                urls = urls_schema.dump(urls).data
                return {'status': 'false', 'urls': urls, 'n_rows': n_rows}

            urls_schema = models.UrlSchema(many=True)
            urls = urls_schema.dump(urls).data
            return {'status': 'ok', 'urls': urls, 'n_rows': n_rows}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def patch(self, url_id, token=''):
        if access(db, models, '/acl/urls', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        urls = db.session.query(models.Urls)
        urls = urls.filter(models.Urls.id == url_id)

        urls.update(args)
        db.session.commit()
        n_rows = urls.count()

        urls = get_urls(db, models)
        urls = urls.filter(models.Urls.id ==url_id)

        urls_schema = models.UrlSchema(many=True)
        urls = urls_schema.dump(urls).data

        return {'n_rows': n_rows, 'urls': urls}
