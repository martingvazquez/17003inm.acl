from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access, get_contractors
import uuid


class Contractors(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/contractors', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='Name cannot be converted', required=True)
        parser.add_argument('last_name', type=str, help='Last name cannot be converted', required=True)
        parser.add_argument('second_last_name', type=str, help='second last name cannot be converted', required=True)
        parser.add_argument('phone_number', type=str, help='Phone number cannot be converted', required=True)
        parser.add_argument('email', type=str, help='Email cannot be converted', required=True)
        parser.add_argument('curp', type=str, help='curp cannot be converted', required=True)
        parser.add_argument('rfc', type=str, help='rfc cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted', required=True)
        parser.add_argument('cp', type=str, help='CP cannot be converted', required=True)
        parser.add_argument('street', type=str, help='Street cannot be converted', required=True)
        parser.add_argument('colony', type=str, help='Colony cannot be converted', required=True)
        parser.add_argument('street_address', type=str, help='Street_address cannot be converted', required=True)
        parser.add_argument('apartment_number', type=str, help='Apartment_number cannot be converted', required=True)
        parser.add_argument('business_name', type=str, help='Business_name cannot be converted', required=True)
        parser.add_argument('legal_entity', type=str, help='Legal_entity cannot be converted', required=True)
        parser.add_argument('contractor_type_id', type=str, help='Contractor_type_id cannot be converted', required=True)
        args = parser.parse_args()

        name = args['name']
        last_name = args['last_name']
        second_last_name = args['second_last_name']
        phone_number = args['phone_number']
        email = args['email']
        curp = args['curp']
        rfc = args['rfc']
        notes = args['notes']
        cp = args['cp']
        street = args['street']
        colony = args['colony']
        street_address = args['street_address']
        apartment_number = args['apartment_number']
        business_name = args['business_name']
        legal_entity = args['legal_entity']
        contractor_type_id = args['contractor_type_id']

        contractors = models.Contractors(name=name, last_name=last_name, second_last_name=second_last_name,
                                         phone_number=phone_number, cp=cp, street=street, colony=colony,
                                         street_address=street_address, apartment_number=apartment_number,
                                         business_name=business_name, legal_entity=legal_entity,
                                         email=email, curp=curp, rfc=rfc, notes=notes, uuid=uuid.uuid4(),
                                         insert_user_uuid=uuid.uuid4(), user_uuid=uuid.uuid4(),
                                         update_user_uuid=uuid.uuid4(), company_uuid=uuid.uuid4(),
                                         contractor_type_id=contractor_type_id)
        try:
            db.session.add(contractors)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        contractor_id = contractors.id
        contractors = get_contractors(db, models)
        contractors = contractors.filter(models.Contractors.id == contractor_id)

        contractor_schema = models.ContractorSchema(many=True)
        contractors = contractor_schema.dump(contractors).data

        return {'status': 'ok','contractors': contractors}

    def get(self, token='', column_order_by='', type_order = '', data_filter='', current_page=1, items_page=5, contractor_id=-1,
            all_show=0, column='', column2='', data_filter2=''):
        if access(db, models, '/acl/contractors', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        contractors = get_contractors(db, models)
        contractors_load = {}

        if contractor_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                contractors = contractors.order_by(order)
                contractors = contractors.filter(
                    or_(
                        models.Contractors.id == condition_int,
                        models.Contractors.name.ilike(condition_string),
                        models.Contractors.last_name.ilike(condition_string),
                        models.Contractors.second_last_name.ilike(condition_string),
                        models.Contractors.phone_number.ilike(condition_string),
                        models.Contractors.email.ilike(condition_string),
                        models.Contractors.curp.ilike(condition_string),
                        models.Contractors.rfc.ilike(condition_string),
                        models.Contractors.notes.ilike(condition_string),
                        models.Contractors.cp.ilike(condition_string),
                        models.Contractors.street.ilike(condition_string),
                        models.Contractors.colony.ilike(condition_string),
                        models.Contractors.street_address.ilike(condition_string),
                        models.Contractors.apartment_number.ilike(condition_string),
                        models.Contractors.business_name.ilike(condition_string),
                        models.Contractors.legal_entity.ilike(condition_string),
                        models.ClientTypes.description.ilike(condition_string)
                        # models.Contractor.status == condition_int
                    )
                )
                contractors = contractors.order_by(order)
                n_rows = contractors.count()

                if all_show == 0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    contractors = contractors.limit(int(items_page)).offset(n_rows_show)
                contractors_load['n_rows'] = n_rows
            else:
                if column == 'contractors.contractor_type_id':
                    contractors = contractors.filter(models.Contractors.contractor_type_id == data_filter)
                if column == 'contractors.status':
                    contractors = contractors.filter(models.Contractors.status == data_filter)
                if column2 != '':
                    if column2 == 'contractors.status':
                        contractors = contractors.filter(models.Contractors.status == data_filter2)

                n_rows = contractors.count()
                contractors_load['n_rows'] = n_rows
        else:
            if contractor_id >= 1:
                contractors = contractors.filter(models.Contractors.id == contractor_id)
                n_rows = contractors.count()
                contractors_load['n_rows'] = n_rows

        contractor_schema = models.ContractorSchema(many=True)
        contractors = contractor_schema.dump(contractors).data
        print(contractors)

        return {'contractors':contractors, 'n_rows':n_rows}

    def put(self, contractor_id, token=''):
        if access(db, models, '/acl/contractors', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        contractors_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='Name cannot be converted', required=True)
        parser.add_argument('last_name', type=str, help='Last name cannot be converted', required=True)
        parser.add_argument('second_last_name', type=str, help='second last name cannot be converted', required=True)
        parser.add_argument('phone_number', type=str, help='Phone number cannot be converted', required=True)
        parser.add_argument('email', type=str, help='Email cannot be converted', required=True)
        parser.add_argument('curp', type=str, help='curp cannot be converted', required=True)
        parser.add_argument('rfc', type=str, help='rfc cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('cp', type=str, help='CP cannot be converted', required=True)
        parser.add_argument('street', type=str, help='Street cannot be converted', required=True)
        parser.add_argument('colony', type=str, help='Colony cannot be converted', required=True)
        parser.add_argument('street_address', type=str, help='Street_address cannot be converted', required=True)
        parser.add_argument('apartment_number', type=str, help='Apartment_number cannot be converted', required=True)
        parser.add_argument('business_name', type=str, help='Business_name cannot be converted', required=True)
        parser.add_argument('legal_entity', type=str, help='Legal_entity cannot be converted', required=True)
        parser.add_argument('contractor_type_id', type=str, help='Contractor_type_id cannot be converted')
        args = parser.parse_args()

        contractors = db.session.query(models.Contractors)
        contractors = contractors.filter(models.Contractors.id == contractor_id)

        n_rows = contractors.count()

        try:
            contractors.update(args)
            db.session.commit()
            contractors = get_contractors(db, models)
            contractors = contractors.filter(models.Contractors.id == contractor_id)
        except Exception as e:
            print(e)
            db.session.rollback()
            contractor_schema = models.ContractorSchema(many=True)
            contractors = contractor_schema.dump(contractors)
            contractors_load['contractors'] = contractors.data
            contractors_load['n_rows'] = n_rows
            contractors_load['status'] = 'false'
            return contractors_load

        print(contractors_load)

        contractor_schema = models.ContractorSchema(many=True)
        contractors = contractor_schema.dump(contractors).data

        return {'status': 'ok', 'contractors': contractors, 'n_rows': n_rows}

    def patch(self, contractor_id, token=''):
        if access(db, models, '/acl/contractors', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        contractors = db.session.query(models.Contractors)
        contractors = contractors.filter(models.Contractors.id == contractor_id)

        contractors.update(args)
        db.session.commit()
        n_rows = contractors.count()

        contractors = get_contractors(db, models)
        contractors = contractors.filter(models.Contractors.id == contractor_id)

        contractor_schema = models.ContractorSchema(many=True)
        contractors = contractor_schema.dump(contractors).data

        return {'n_rows': n_rows, 'contractors': contractors}
