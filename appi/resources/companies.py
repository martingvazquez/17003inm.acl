from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, \
    get_data_filter, get_companies, access, upper, language
import uuid
from datetime import datetime, timedelta


class Companies(Resource):

    def post(self, token):
        if access(db, models, '/acl/companies', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', required=True)
            parser.add_argument('notes', type=str)
            parser.add_argument('city_id', type=int, required=True)
            args = parser.parse_args()
            description = args['description']
            notes = args['notes']
            city_id = args['city_id']

            if upper == 1:
                description = description.upper()
                if notes is not None:
                    notes = notes.upper()

            companies = models.Companies(description=description, notes=notes,
                                         uuid=uuid.uuid4(), city_id=city_id)
            try:
                db.session.add(companies)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                if language == 1:
                    return {'error': '5', 'message': 'Error descripción repetida'}
                else:
                    return {'error': '5', 'message': 'Error description repeat'}

            company_id = companies.id
            companies = get_companies(db, models)
            companies = companies.filter(models.Companies.id == company_id)

            companies_schema = models.CompanySchema(many=True)
            companies = companies_schema.dump(companies).data

            return {'status': 'ok', 'companies': companies}
        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def get(self, token='', column_order_by='', type_order='', data_filter='',
            current_page=1, items_page=5, company_id=-1, show_all=0, column='', column2='',
            data_filter2=''):

        if access(db, models, '/acl/companies', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401
        companies = get_companies(db, models)
        companies_load = {}

        if company_id == -1:

            if column == '':

                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                companies = companies.order_by(order)
                companies = companies.filter(
                    or_(
                        models.Companies.id == condition_int,
                        models.Companies.description.ilike(condition_string),
                        models.Cities.description.ilike(condition_string),
                        models.States.description.ilike(condition_string),
                        models.Countries.description.ilike(condition_string),
                        models.Companies.notes.ilike(condition_string)
                    )
                )


                n_rows = companies.count()

                if show_all == 0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    companies = companies.limit(int(items_page)).offset(n_rows_show)
                companies_load['n_rows'] = n_rows
            else:
                if column == 'companies.status':
                    companies = companies.filter(models.Companies.status == data_filter)

                if column == 'companies.country_id':
                    companies = companies.filter(models.Companies.country_id == data_filter)
                if column2 != '':
                    if column2 == 'companies.status':
                        companies = companies.filter(models.Companies.status == data_filter2 )

                n_rows = companies.count()
                companies_load['n_rows'] = n_rows
        else:
            if company_id >= 1:
                companies = companies.filter(models.Companies.id == company_id)
                n_rows = companies.count()
                companies_load['n_rows'] = n_rows

        companies_schema = models.CompanySchema(many=True)
        companies = companies_schema.dump(companies)
        companies_load['companies'] = companies.data


        return companies_load

    def put(self, company_id, token):
        if access(db, models, '/acl/companies', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        try:
            parser.add_argument('description', help='Description cannot be converted', required=True)
            parser.add_argument('notes', type=str)
            parser.add_argument('city_id', type=int, required=True)

            args = parser.parse_args()

            companies = db.session.query(models.Companies)
            companies = companies.filter(models.Companies.id == company_id)
            n_rows = companies.count()

            try:
                companies.update(args)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                companies = get_companies(db, models)
                companies = companies.filter(models.Companies.id == company_id)
                companies_schema = models.CompanySchema(many=True)
                companies = companies_schema.dump(companies).data
                return {'error': '5', 'message':'Error descripción repeat', 'companies': companies}

            companies = get_companies(db, models)
            companies = companies.filter(models.Companies.id == company_id)

            companies_schema = models.CompanySchema(many=True)
            companies = companies_schema.dump(companies).data

            return {'status': 'ok', 'companies': companies}

        except Exception as e:
            print(e)
            return {'error': '406', 'message': 'Conflict Missing required data'}, 406

    def patch(self, company_id, token):
        if access(db, models, '/acl/companies', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized '}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Description cannot be converted', required=True)
        args = parser.parse_args()

        companies = db.session.query(models.Companies)
        companies = companies.filter(models.Companies.id == company_id)

        companies.update(args)
        db.session.commit()
        n_rows = companies.count()

        companies = get_companies(db, models)
        companies = companies.filter(models.Companies.id ==company_id)

        companies_schema = models.CompanySchema(many=True)
        companies = companies_schema.dump(companies).data

        return {'n_rows': n_rows, 'companies':companies}