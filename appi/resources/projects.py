from flask_restful import Resource, reqparse
from application import models, db, or_, get_order_by, get_data_filter, access, get_projects
import uuid


class Projects(Resource):

    def post(self, token=''):
        if access(db, models, '/acl/projects', 'post', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted', required=True)
        parser.add_argument('cp', type=str, help='CP cannot be converted', required=True)
        parser.add_argument('street', type=str, help='Street cannot be converted', required=True)
        parser.add_argument('colony', type=str, help='Colony cannot be converted', required=True)
        parser.add_argument('street_address', type=str, help='Street_address cannot be converted', required=True)
        parser.add_argument('apartment_number', type=str, help='Apartment_number cannot be converted', required=True)
        parser.add_argument('project_type_id', type=str, help='Project_type_id cannot be converted', required=True)
        parser.add_argument('contractor_id', type=str, help='Contractor_id cannot be converted', required=True)

        args = parser.parse_args()

        description = args['description']
        notes = args['notes']
        cp = args['cp']
        street = args['street']
        colony = args['colony']
        street_address = args['street_address']
        apartment_number = args['apartment_number']
        project_type_id = args['project_type_id']
        contractor_id = args['contractor_id']

        projects = models.Projects(description=description,
                                   notes=notes,
                                   cp=cp, street=street,
                                   colony=colony,
                                   street_address=street_address,
                                   apartment_number=apartment_number,
                                   uuid=uuid.uuid4(),
                                   insert_user_uuid=uuid.uuid4(),
                                   update_user_uuid=uuid.uuid4(),
                                   company_uuid=uuid.uuid4(),
                                   project_type_id=project_type_id,
                                   contractor_id=contractor_id)

        print(projects)
        try:
            db.session.add(projects)
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            return {'status': 'false'}

        project_id = projects.id
        projects = get_projects(db, models)
        projects = projects.filter(models.Projects.id == project_id)

        projects_schema = models.projectSchema(many=True)
        projects = projects_schema.dump(projects).data

        return {'status': 'ok', 'projects': projects}

    def get(self, token='', column_order_by='', type_order='', data_filter='', current_page=1, items_page=5, project_id=-1,
            all_show=0, column='', column2='', data_filter2=''):
        if access(db, models, '/acl/projects', 'get', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401

        projects = get_projects(db, models)
        projects_load = {}

        if project_id == -1:
            if column == '':
                order = get_order_by(column_order_by, type_order)
                data_filter_integer, data_filter_string = get_data_filter(data_filter)

                condition_string = '%'+data_filter_string+'%'
                condition_int = data_filter_integer

                projects = projects.order_by(order)

                projects = projects.filter(
                    or_(
                        models.Projects.id == condition_int,
                        models.Projects.description.ilike(condition_string),
                        models.Projects.notes.ilike(condition_string),
                        models.Projects.cp.ilike(condition_string),
                        models.Projects.street.ilike(condition_string),
                        models.Projects.colony.ilike(condition_string),
                        models.Projects.street_address.ilike(condition_string),
                        models.Projects.apartment_number.ilike(condition_string),
                        models.Project_types.description.ilike(condition_string),
                        models.Contractors.name.ilike(condition_string)
                        # models.Projects.status == condition_int
                    )
                )

                projects = projects.order_by(order)
                n_rows = projects.count()

                if all_show == 0:
                    n_rows_show = (int(current_page)-1) * int(items_page)
                    projects = projects.limit(int(items_page)).offset(n_rows_show)
                projects_load['n_rows'] = n_rows
            else:
                if column == 'projects.status':
                    projects = projects.filter(models.Projects.status == data_filter)
                    n_rows = projects.count()
                    projects_load['n_rows'] = n_rows
        else:
            if project_id >= 1:
                projects = projects.filter(models.Projects.id == project_id)
                n_rows = projects.count()
                projects_load['n_rows'] = n_rows

        projects_schema = models.projectSchema(many=True)
        projects = projects_schema.dump(projects).data
        print(projects)

        return {'projects':projects, 'n_rows':n_rows}

    def put(self, project_id, token=''):
        if access(db, models, '/acl/projects', 'put', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        projects_load = {}
        parser = reqparse.RequestParser()
        parser.add_argument('description', type=str, help='description cannot be converted', required=True)
        parser.add_argument('notes', type=str, help='Notes cannot be converted')
        parser.add_argument('cp', type=str, help='CP cannot be converted', required=True)
        parser.add_argument('street', type=str, help='Street cannot be converted', required=True)
        parser.add_argument('colony', type=str, help='Colony cannot be converted', required=True)
        parser.add_argument('street_address', type=str, help='Street_address cannot be converted', required=True)
        parser.add_argument('apartment_number', type=str, help='Apartment_number cannot be converted', required=True)
        parser.add_argument('project_type_id', type=str, help='Project_type_id cannot be converted')
        parser.add_argument('contractor_id', type=str, help='Contractor_id cannot be converted', required=True)
        args = parser.parse_args()

        projects = db.session.query(models.Projects)
        projects = projects.filter(models.Projects.id == project_id)

        n_rows = projects.count()

        try:
            projects.update(args)
            db.session.commit()
            projects = get_projects(db, models)
            projects = projects.filter(models.Projects.id == project_id)
        except Exception as e:
            print(e)
            db.session.rollback()
            projects_schema = models.projectSchema(many=True)
            projects = projects_schema.dump(projects)
            projects_load['projects'] = projects.data
            projects_load['n_rows'] = n_rows
            projects_load['status'] = 'false'
            return projects_load

        projects_schema = models.projectSchema(many=True)
        projects = projects_schema.dump(projects).data

        return {'status': 'ok', 'projects': projects, 'n_rows': n_rows}

    def patch(self, project_id, token=''):
        if access(db, models, '/acl/projects', 'patch', token) == -1:
            return {'error': '401', 'message': 'Unauthorized'}, 401
        parser = reqparse.RequestParser()
        parser.add_argument('status', help='Status cannot be converted', required=True)
        args = parser.parse_args()

        projects = db.session.query(models.Projects)
        projects = projects.filter(models.Projects.id == project_id)

        projects.update(args)
        db.session.commit()
        n_rows = projects.count()

        projects = get_projects(db, models)
        projects = projects.filter(models.Projects.id == project_id)

        projects_schema = models.projectSchema(many=True)
        projects = projects_schema.dump(projects).data

        return {'n_rows': n_rows, 'projects': projects}