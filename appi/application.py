from sqlalchemy.dialects.postgresql import UUID
from datetime import datetime
from functions import get_order_by,access, get_data_filter, get_states,get_cities,\
    get_companies, get_groups,get_resources, get_urls, get_users,get_tokens, \
    access_external_application, review_token, get_agents, get_clients, \
    get_phase_financing_types, get_projects, get_documents, get_financings, \
    get_locations, get_contractors
from config import *
from sqlalchemy import or_, UniqueConstraint, func
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import fields
app = Flask(__name__)
app.config.from_pyfile('../config.py')
db = SQLAlchemy(app)
ma = Marshmallow(app)

upper = upper()
language = language()

from models import models

from resources.companies import Companies
from resources.countries import Countries
from resources.states import States
from resources.cities import Cities
from resources.applications import Applications
from resources.groups_urls import Groups_Urls
from resources.groups import Groups
from resources.resources import Resources
from resources.urls import Urls
from resources.users import Users
from resources.sessions import Sessions
from resources.tokens import Tokens

from resources.client import Clients
from resources.client_types import ClientTypes
from resources.financings import Financings
from resources.financing_types import FinancingTypes
from resources.phases import Phases
#from resources.phase_financing_types import PhaseFinancingTypes
from resources.document_types import DocumentTypes
from resources.documents import Documents
from resources.agents import Agents
from resources.agent_types import AgentTypes
from resources.project_types import ProjectTypes
from resources.contractor_types import ContractorTypes
from resources.contractors import Contractors
from resources.projects import Projects
from resources.locations import Locations


api = Api(app)
api.add_resource(Companies,
                 '/companies/token/<string:token>',
                 '/companies/token/<string:token>/show_all/<int:show_all>',
                 '/companies/token/<string:token>/<string:column_order_by>/<string:'
                 'type_order>/<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/companies/token/<string:token>/<int:company_id>',
                 '/companies/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/companies/token/<string:token>/filters2/<string:column>/<string:'
                 'data_filter>/<string:column2>/<string:data_filter2>'
                 )

api.add_resource(Tokens,
                 '/tokens/review_tokens/<int:other_application>',
                 '/tokens/token/<string:token>/application/<string:application>/resource/<string:'
                 'resource>/method/<string:method>')

api.add_resource(Countries,
                 '/countries/token/<string:token>',
                 '/countries/token/<string:token>/show_all/<int:show_all>',
                 '/countries/token/<string:token>/<string:column_order_by>/<string:'
                 'type_order>/<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/countries/token/<string:token>/<int:country_id>',
                 '/countries/token/<string:token>/filter/<string:column>/<string:data_filter>'
                 )
api.add_resource(States,
                 '/states/token/<string:token>',
                 '/states/token/<string:token>/show_all/<int:show_all>',
                 '/states/token/<string:token>/<string:column_order_by>/<string:type_order>/<string:'
                 'data_filter>/<int:current_page>/<int:items_page>',
                 '/states/token/<string:token>/<int:state_id>',
                 '/states/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/states/token/<string:token>/filters2/<string:column>/<string:data_filter>/<string:'
                 'column2>/<string:data_filter2>'
                 )

api.add_resource(Users,
                 '/users/token/<string:token>',
                 '/users/token/<string:token>/show_all/<int:show_all>',
                 '/users/token/<string:token>/<string:column_order_by>/<string:type_order>/<string:'
                 'data_filter>/<int:current_page>/<int:items_page>',
                 '/users/token/<string:token>/<int:user_id>',
                 '/users/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/users/token/<string:token>/filters2/<string:column>/<string:data_filter>/<string:'
                 'column2>/<string:data_filter2>'
                 )


api.add_resource(Cities,
                 '/cities/token/<string:token>',
                 '/cities/token/<string:token>/show_all/<int:show_all>',
                 '/cities/token/<string:token>/<string:column_order_by>/<string:type_order>/<string:'
                 'data_filter>/<int:current_page>/<int:items_page>',
                 '/cities/token/<string:token>/<int:city_id>',
                 '/cities/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/cities/token/<string:token>/filters2/<string:column>/<string:data_filter>/<string:'
                 'column2>/<string:data_filter2>')
api.add_resource(Applications,
                 '/applications/token/<string:token>',
                 '/applications/token/<string:token>/show_all/<int:show_all>',
                 '/applications/token/<string:token>/<string:column_order_by>/<string:'
                 'type_order>/<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/applications/token/<string:token>/<int:application_id>',
                 '/applications/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/applications/token/<string:token>/filters2/<string:column>/<string:'
                 'data_filter>/<string:column2>/<string:data_filter2>')
api.add_resource(Groups_Urls,
                 '/groups_urls',
                 '/groups_urls/group/<int:group_id>')
api.add_resource(Groups,
                 '/groups/token/<string:token>',
                 '/groups/token/<string:token>/show_all/<int:show_all>',
                 '/groups/token/<string:token>/<string:column_order_by>/<string:type_order>/<string:'
                 'data_filter>/<int:current_page>/<int:items_page>',
                 '/groups/token/<string:token>/<int:group_id>',
                 '/groups/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/groups/token/<string:token>/filters2/<string:column>/<string:'
                 'data_filter>/<string:column2>/<string:data_filter2>',
                 '/groups/token/<string:token>/permissions/<int:group_id>/<int:permission>'
                 )
api.add_resource(Resources,
                 '/resources/token/<string:token>',
                 '/resources/token/<string:token>/show_all/<int:show_all>',
                 '/resources/token/<string:token>/<string:column_order_by>/<string:'
                 'type_order>/<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/resources/token/<string:token>/<int:resource_id>',
                 '/resources/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/resources/token/<string:token>/filters2/<string:column>/<string:'
                 'data_filter>/<string:column2>/<string:data_filter2>')
api.add_resource(Urls,
                 '/urls/token/<string:token>',
                 '/urls/token/<string:token>/show_all/<int:show_all>',
                 '/urls/token/<string:token>/<string:column_order_by>/<string:'
                 'type_order>/<string:data_filter>/<int:'
                 'current_page>/<int:items_page>',
                 '/urls/token/<string:token>/<int:url_id>',
                 '/urls/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/urls/token/<string:token>filters2/<string:column>/<string:data_filter>/<string:column2>/<string:'
                 'data_filter2>')
api.add_resource(Sessions,
                 '/sessions/valid/<string:user>/<string:password>',
                 '/sessions/<string:token>'
                 )

api.add_resource(Phases,
                 '/phases/token/<string:token>',
                 '/phases/token/<string:token>/all/<int:all>',
                 '/phases/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/phases/token/<string:token>/<int:phase_id>',
                 '/phases/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(AgentTypes,
                 '/agent-types/token/<string:token>',
                 '/agent-types/token/<string:token>/all/<int:all>',
                 '/agent-types/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/agent-types/token/<string:token>/<int:agent_type_id>',
                 '/agent-types/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(Agents,
                 '/agents/token/<string:token>',
                 '/agents/token/<string:token>/all/<int:all>',
                 '/agents/token/<string:token>/<string:column_order_by>/<string:type_order>/<string:data_filter>/<int:'
                 'current_page>/<int:items_page>',
                 '/agents/token/<string:token>/<int:agent_id>',
                 '/agents/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(Clients,
                 '/clients/token/<string:token>',
                 '/clients/token/<string:token>/all/<int:all>',
                 '/clients/token/<string:token>/<string:column_order_by>/<string:type_order>/<string:'
                 'data_filter>/<int:current_page>/<int:items_page>',
                 '/clients/token/<string:token>/<int:client_id>',
                 '/clients/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/clients/token/<string:token>/filters2/<string:column>/<string:data_filter>/<string:'
                 'column2>/<string:data_filter2>',
                 '/clients/token/<string:token>/locations/<int:client_id>/<int:location>')

api.add_resource(ClientTypes,
                 '/client-types/token/<string:token>',
                 '/client-types/token/<string:token>/all/<int:all>',
                 '/client-types/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/client-types/token/<string:token>/<int:client_type_id>',
                 '/client-types/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(FinancingTypes,
                 '/financing-types/token/<string:token>',
                 '/financing-types/token/<string:token>/all/<int:all>',
                 '/financing-types/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/financing-types/token/<string:token>/<int:financing_type_id>',
                 '/financing-types/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(Financings,
                 '/financings/token/<string:token>',
                 '/financings/token/<string:token>/all/<int:all>',
                 '/financings/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/financings/token/<string:token>/<int:financing_id>',
                 '/financings/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/financings/token/<string:token>/filters2/<string:column>/<string:data_filter>/'
                 '<string:column2>/<string:data_filter2>')

api.add_resource(ContractorTypes,
                 '/contractor-types/token/<string:token>',
                 '/contractor-types/token/<string:token>/all/<int:all>',
                 '/contractor-types/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/contractor-types/token/<string:token>/<int:contractor_type_id>',
                 '/contractor-types/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(Contractors,
                 '/contractors/token/<string:token>',
                 '/contractors/token/<string:token>/all/<int:all>',
                 '/contractors/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/contractors/token/<string:token>/<int:contractor_id>',
                 '/contractors/token/<string:token>/filter/<string:column>/<string:data_filter>',
                 '/contractors/token/<string:token>/filters2/<string:column>/<string:data_filter>/'
                 '<string:column2>/<string:data_filter2>')

api.add_resource(DocumentTypes,
                 '/document-types/token/<string:token>',
                 '/document-types/token/<string:token>/all/<int:all>',
                 '/document-types/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/document-types/token/<string:token>/<int:document_type_id>',
                 '/document-types/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(Documents,
                 '/documents/token/<string:token>',
                 '/documents/token/<string:token>/all/<int:all>',
                 '/documents/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/documents/token/<string:token>/<int:document_id>',
                 '/documents/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(Locations,
                 '/locations/token/<string:token>',
                 '/locations/token/<string:token>/all/<int:all>',
                 '/locations/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/locations/token/<string:token>/<int:location_id>',
                 '/locations/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(ProjectTypes,
                 '/project-types/token/<string:token>',
                 '/project-types/token/<string:token>/all/<int:all>',
                 '/project-types/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/project-types/token/<string:token>/<int:project_type_id>',
                 '/project-types/token/<string:token>/filter/<string:column>/<string:data_filter>')

api.add_resource(Projects,
                 '/projects/token/<string:token>',
                 '/projects/token/<string:token>/all/<int:all>',
                 '/projects/token/<string:token>/<string:column_order_by>/<string:type_order>/'
                 '<string:data_filter>/<int:current_page>/<int:items_page>',
                 '/projects/token/<string:token>/<int:project_id>',
                 '/projects/token/<string:token>/filter/<string:column>/<string:data_filter>')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5000)
